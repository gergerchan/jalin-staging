import "./asset/css/general.scss"
import { useState } from 'react'
import AdminRoute from "./routing/AdminRoute";
function App() {
  const [qrcodeModal, setQrcodeModal] = useState(false);

  const toggleQrcodeModal = () => setQrcodeModal(!qrcodeModal);

  const closeQrcodeBtn = <button className="btn-close" onClick={toggleQrcodeModal}></button>;

  return (
    <div className="base">
        <AdminRoute />
    </div>
  );
}

export default App;
