import { Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import "./usercheckin.scss"
import { useState } from 'react';
const UserCheckin = (props) => {
    let string
    const [datacheckin, setdatacheckin] = useState(
        props.data.map(data => {
            string = data.cid.includes("-") ? data.cid.split("-") : data.cid.split(".")
            string = string[string.length-1]
            return { cid:string, name :data.name,timeMark:data.timeMark}
        })
    )

    return (
        <div className='base home-user-app-checkin my-5'>
            <div className="row home-user-app-checkin-heading">
                <h5 className="col-8 home-user-app-checkin-title">User’s App Check In</h5>
                <Link className="col-4 home-user-app-checkin-viewall" to='/leaderboard/checkin'>View all</Link>
            </div>
            <div className='user-checkin-data-table'>
                <Table borderless>
                    <thead className='user-checkin-data-table-heading'>
                        <tr>
                            <th className='user-checkin-left-heading ps-4 py-3'>CID</th>
                            <th className='user-checkin-mid-heading ps-4 py-3'>Name</th>
                            <th className='user-checkin-right-heading ps-4 py-3'>Time Mark</th>
                        </tr>
                    </thead>
                    <tbody className='user-checkin-data-table-items'>
                    {(datacheckin.length === 0) ? 
                        <tr>
                            <td className='user-recent-transaction-row ps-4 py-3'>No checkin today</td>
                        </tr>
                        : datacheckin.map((data,i) =>
                        <tr>
                            <td className='user-recent-transaction-row ps-4 py-3'>{data.cid}</td>
                            <td className='user-recent-transaction-row ps-4 py-3'>{data.name}</td>
                            <td className='user-recent-transaction-row ps-4 py-3'>{data.timeMark}</td>
                        </tr>
                        )}
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default UserCheckin