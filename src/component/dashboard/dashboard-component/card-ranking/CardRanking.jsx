import { RiBarChart2Fill } from "react-icons/ri";
import FotoProfileFirstMedal from "../../../../asset/img/first-medal-foto-profile.svg"
import FotoProfileSecondMedal from "../../../../asset/img/second-medal-foto-profile.svg"
import FotoProfileThirdMedal from "../../../../asset/img/third-medal-foto-profile.svg"
import IconFirstMedal from "../../../../asset/img/first-medal.png"
import IconSecondMedal from "../../../../asset/img/second-medal.png"
import IconThirdMedal from "../../../../asset/img/third-medal.png"
import './cardRanking.scss'

const CardRanking = (props) => {
    return (
        <div className="card-ranking">
            <div className="row pt-4 pe-4 header-card-ranking">
                <div className="col title align-self-center">
                    <RiBarChart2Fill className="icon-leaderboard" />
                    <h5>Leaderboard</h5>
                </div>
                {/* <div className="col">
                    <div className="button-header">
                        <button type="button" class="btn button-header-item">Today</button>
                        <div className="divider"></div>
                        <button type="button" class="btn button-header-item">Last Week</button>
                        <div className="divider"></div>
                        <button type="button" class="btn button-header-item">Last Month</button>
                    </div>
                </div> */}
                {/* <div className="col align-self-center text-end">
                    <div className="date-select">
                        <form action="/action_page.php">
                            <label for="date">Select Date</label>
                            <input class="input-date py-1" type="date" id="date" name="date" />
                            <input type="submit" />
                        </form>
                    </div>
                </div> */}
            </div>
            <div className="card-deck reward-card-rank ">
                <div className="row p-3">
                    <div className="col d-flex justify-content-end align-self-end">
                        <div class="card card-reward">
                            <img src={IconSecondMedal} class="icon-medal" alt="medal-icon" />
                            <img src={FotoProfileFirstMedal} class="card-img-top mt-4 " alt="profile-medal"></img>
                            <div class="card-body  text-center">
                                <p class="card-text mb-1">{props.data[1].jalinId.split("-")[0]}</p>
                                <h5 class="card-title b-700">{props.data[1].totalPoints}</h5>
                                <p class="card-text"><small class="text-muted">Points</small></p>
                            </div>
                        </div>
                    </div>
                    <div className="col-3 p-0 d-flex justify-content-center">
                        <div class="card card-reward-first text-center">
                            <img src={IconFirstMedal} class="icon-medal" alt="medal-icon" />
                            <img src={FotoProfileSecondMedal} class="card-img-top mt-5" alt="medal"></img>
                            <div class="card-body">
                                <p class="card-text mb-1">{props.data[0].jalinId.split("-")[0]}</p>
                                <h5 class="card-title b-700">{props.data[0].totalPoints}</h5>
                                <p class="card-text"><small class="text-muted">Points</small></p>
                            </div>
                        </div>
                    </div>
                    <div className="col d-flex justify-content-start align-self-end">
                        <div class="card card-reward text-center">
                            <img src={IconThirdMedal} class="icon-medal" alt="medal-icon" />
                            <img src={FotoProfileThirdMedal} class="card-img-top mt-4" alt="medal"></img>
                            <div class="card-body">
                                <p class="card-text mb-1">{props.data[2].jalinId.split("-")[0]}</p>
                                <h5 class="card-title b-700">{props.data[2].totalPoints}</h5>
                                <p class="card-text"><small class="text-muted">Points</small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardRanking
