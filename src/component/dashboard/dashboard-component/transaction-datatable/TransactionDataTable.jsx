import ReactDatatable from '@ashvin27/react-datatable';
import "./tableTransaction.scss"
import "./transaction-style.scss"
import { FaClipboardList } from "react-icons/fa";
import { useState } from 'react';
import { dateNow } from '../../../../services/helpers';
import TransactionCard from '../transaction-card/TransactionCard';
import { useSelector, useDispatch } from "react-redux";
import { transactionReceipt,filterTrans } from '../../../../redux/transaction-action';
import { Spinner } from "reactstrap"
import moment from 'moment';

const TransactionDataTable = (props) => {
    const dispatch = useDispatch()
    const [firstDate, setFirstDate] = useState(moment().subtract(1, "week").format("YYYY-MM-DD"))
    const [secondDate, setSecondDate] = useState(dateNow())
    const [transType,SetTransType] = useState("none")
    const [mutation,SetMutation] = useState("none")
    const dataTrans = useSelector(state => state.transaction)
    const tableLoading = useSelector(state => state.isLoading1)

    const columns = [
        {
            key: "id",
            text: "ID TRANSACTION",
            sortable: true
        },
        {
            key: "Transaction",
            text: "TRANSACTION",
            sortable: true
        },
        {
            key: "Destination",
            text: "DESTINATION",
            sortable: true
        },
        {
            key: "Date",
            text: "DATE",
            sortable: true
        },
        {
            key: "Time",
            text: "TIME",
            sortable: true
        },
        {
            key: "CashFlow",
            text: "CASH FLOW",
            sortable: true
        }
    ]

    const config = {
        page_size: 5,
        length_menu: [5, 10, 20, 50],
        show_filter: true,
        show_pagination: true,
        pagination: 'basic',
        filename: "Transaction Table",
        button: {
            csv: true
        }
    }

    const rowClickedHandler = (event, data, rowIndex) => {
        dispatch(transactionReceipt(data))
    }


    const handleChangeFirstDate = (e) => {
        e.preventDefault();
        const startDate = (secondDate < e.target.value) ?
        secondDate : e.target.value
        setFirstDate(startDate)
    }

    const handleChangeSecondDate = (e) => {
        e.preventDefault()
        const endDate = (firstDate > e.target.value) ?
        firstDate: e.target.value
        setSecondDate(endDate)
    }

    const handleMutation = (e)  => {
        e.preventDefault()
        SetMutation(e.target.value)
    }
    const handleTransType = (e)  => {
        e.preventDefault()
        SetTransType(e.target.value)
    }
    const handleApply = ()  => {
        const dataFilter = {
            startDate:firstDate,
            endDate:secondDate,
            mutation:mutation,
            transType:transType
        }
        dispatch(filterTrans(dataFilter))
    }

    return (
        <>
        <div className="trans-style">
            <div className="row">
                <div className="col mt-4">
                    <div className="icon-mission align-items-center">
                        <FaClipboardList color="#287CA1" size="28px" className="transaction-icon"/>
                        <span className="transaction-title ms-2">Transaction</span>
                    </div>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-5">
                    <select class="transaction-form-select py-2 px-3" onChange={handleMutation} aria-label="Default select example" >
                        <option value="none" disabled selected>Select mutation</option>
                        <option value="none">All</option>
                        <option value="D">Debit</option>
                        <option value="C">Credit</option>
                    </select>
                </div>
                <div className="col-7 p-0 d-flex justify-content-end">
                    <div className="date-container">
                        <input type="date" value={firstDate} className="date-table date-table-left" onChange={handleChangeFirstDate} />
                        <span className="text-to">To</span>
                        <input type="date" value={secondDate} className="date-table date-table-right" onChange={handleChangeSecondDate} />
                    </div>
                </div>
            </div>
            {tableLoading && <Spinner color="primary" children="" className="mt-4" />}
            {!tableLoading && dataTrans && <>
            <div className="row mt-4">
                <div className="col-6"><TransactionCard color="color-black" title="Total Transactions" number={dataTrans.length}/></div>
                <div className="col-6"><TransactionCard color="color-red" title="Failed Transactions" number="0"/></div>
                <div className="col-6"><TransactionCard color="color-green" title="Success Transactions" number={dataTrans.length}/></div>
                <div className="col-6"><TransactionCard color="color-blue"  title="Pending Transactions" number="0"/></div>
            </div>
            </>
            }
            <div className="row mt-4 d-flex justify-content-between">
                <div className="col-6">
                    <select class="transaction-form-select py-2 px-3" onChange={handleTransType} aria-label="Default select example" >
                        <option value="none" disabled selected>Transaction Type</option>
                        <option value="none">All</option>
                        <option value="PAYMENT_QR">Pembayaran QR</option>
                        <option value="TOP_UP">Top Up E-Wallet</option>
                        <option value="TRANSFER">Transfer</option>
                        {/* <option value="PAYMENT_BILL">Pembayaran Tagihan</option> */}
                        <option value="TRANSFER_DOMESTIC">Transfer Domestik</option>
                        <option value="PAYMENT_MOBILE_PHONE_CREDIT">Isi Pulsa</option>
                        {/* <option value="PAYMENT_MOBILE_PHONE_DATA">Isi Paket Data</option> */}
                    </select>
                </div>
                <div className="col-6 d-flex justify-content-end">
                    <button onClick={handleApply} className="transaction-form-button py-2 px-4">Apply All</button>
                </div>
            </div>
        </div>
        {tableLoading && <Spinner color="primary" children="" className="mt-4" />}
        {!tableLoading && dataTrans && <>
        <div className="data-table-trans mt-3 ">
            <div className="transaction mt-4 mb-5">
                <div className="table-transaction">
                    <ReactDatatable
                        config={config}
                        records={dataTrans}
                        columns={columns}
                        onRowClicked={rowClickedHandler} />
                </div>
            </div>
        </div>
        </>
        }
        </>
    )

}

export default TransactionDataTable