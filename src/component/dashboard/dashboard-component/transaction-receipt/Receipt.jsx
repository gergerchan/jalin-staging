import { FaReceipt } from "react-icons/fa";
import "./transaction-receipt.scss"
import receiptImg from "../../../../asset/img/transaction-receipt.svg"
import { useSelector} from "react-redux";
import { Spinner } from "reactstrap"
import img from "../../../../asset/img/pencil.svg"
import React, { createRef } from "react";
import domtoimage from 'dom-to-image';
import { saveAs } from 'file-saver';

const Receipt = () => {
    const data = useSelector(state => state.transactionreceipt)
    const loading = useSelector(state => state.isLoading2)
    const ref = createRef(null);

    const download = (event) => {
        event.preventDefault();
        domtoimage.toBlob(document.getElementById('my-node'))
        .then(function (blob) {
            saveAs(blob, 'myImage.png');
        });
    }
    
    return (
        <>  
            {loading ?
                <Spinner color="primary" children="" className="mt-4" />
                : !data &&
                <div className="transaction-receipt">
                    <p className="transaction-receipt-title b-700">Show Receipt</p>
                    <img src={receiptImg} className="mt-3" alt="jalin-receipt" />
                    <p className="transaction-receipt-subtitle mt-3">See the details information about a 
                        transaction by pointing your mouse to the selected 
                        transaction in the transaction list.
                    </p>
                </div>
            }
            {
            !loading && data  &&
                <div className="transaction-receipt">
                    <FaReceipt color="#142B6F" size="28px"/>
                    <span className="transaction-receipt-title ms-2 b-700">Receipt</span>

                    <div ref={ref} id="my-node" className="transaction-receipt-card p-3 mt-3">
                        <div className="row ">
                            <div className="col-6  d-flex justify-content-start"><span className="transaction-receipt-id p-2">{data.transactionId}</span></div>
                            <div className="col-6 d-flex justify-content-end"><span className="transaction-receipt-status b-700 py-2 px-3">Success</span></div>
                            <div className="col-12 mt-3"><span className="transaction-receipt-subtitle" >Date</span></div>
                            <div className="col-5">{data.transactionDate}</div>
                            <div className="col-7">{data.transactionTime}</div>
                            <div className="col-12 mt-2">
                                <p className="transaction-receipt-subtitle" >Account holder name:</p>
                                <p>Jalin On Account - {data.sourceAccountNumber}</p>
                                <p>{data.sourceAccountCustomerName}</p>
                            </div>
                            <div className="col-12 mt-2">
                                <p className="transaction-receipt-subtitle" >Transfer To:</p>
                                {(data.beneficiaryAccountNumber === "null") ?
                                <p>{data.corporateName} </p> :  
                                data.beneficiaryAccountCustomerName ?
                                <>
                                <p>{data.corporateName} - {data.beneficiaryAccountNumber}</p> 
                                <p>{data.beneficiaryAccountCustomerName} </p>
                                </> :
                                <p>{data.corporateName} - {data.beneficiaryAccountNumber}</p>
                                }
                            </div>
                            <div className="col-12 transaction-receipt-line mt-2 pb-2">
                                <p className="transaction-receipt-subtitle" >Description:</p>
                                <p>{data.transactionNote}</p>
                            </div>
                            <div className="col-4 mt-2">
                                <span className="transaction-receipt-subtitle" >Amount:</span>
                            </div>
                            <div className="col-8 mt-2 d-flex justify-content-end">
                                <span className="transaction-receipt-amount b-700" >{data.amount}</span>
                            </div>
                            <div className="col-12 mt-3">
                            <img src={img} className="transaction-receipt-download" onClick={download} alt="download" />
                            </div>
                        </div>
                    </div>
                </div> 
            }
        </>
    )
}

export default Receipt
