import { useState,useEffect, useRef } from 'react';
import ReactDatatable from '@ashvin27/react-datatable';
import IconListMission from '../../../../asset/img/icon_list_mission.png'
import './TableUserCheckin.scss'
import { dateNow } from '../../../../services/helpers';
import { useDispatch, useSelector } from 'react-redux'
import { tableCheckin } from  "../../../../redux/point-action"
import { Spinner } from 'reactstrap';
import {  useParams } from 'react-router-dom'

const TableUserCheckin = (props) => {
    const dispatch = useDispatch()
    let { name } = useParams();
    const [firstDate, setFirstDate] = useState(dateNow())
    const [secondDate, setSecondDate] = useState(dateNow())

    const checkinloading = useSelector(state => state.isLoading2)
    const checkinData = useSelector(state=> state.tableCheckin)

    const myRef = useRef();

    const executeScroll = () => myRef.current.scrollIntoView()   
  
    useEffect(() => {
        dispatch(tableCheckin())
        name === "checkin" && executeScroll()
    }, [])

    const columns = [
        {
            key: "cid",
            text: "CID",
            sortable: true
        },
        {
            key: "name",
            text: "FULL NAME",
            sortable: true
        },
        {
            key: "pointsUnlocked",
            text: "POINTS UNLOCKED",
            sortable: true
        },
        {
            key: "date",
            text: "DATE",
            sortable: true
        },
        {
            key: "timeMark",
            text: "TIME MARK",
            sortable: true
        }
    ]

    const config = {
        page_size: 10,
        length_menu: [10, 20, 50],
        show_filter: true,
        show_pagination: true,
        pagination: 'basic',
        button: {
            csv: true
        }
    }

    const handleChangeFirstDate = (e) => {
        const startDate = (secondDate < e.target.value) ?
        secondDate : e.target.value
        setFirstDate(startDate)
        let date = {
            start: startDate,
            end:secondDate
        }
        dispatch(tableCheckin(date))
    }

    const handleChangeSecondDate = (e) => {
        const endDate = (firstDate > e.target.value) ?
        firstDate: e.target.value
        setSecondDate(endDate)
        let date = {
            start:firstDate,
            end:endDate
        }
        dispatch(tableCheckin(date))
    }

    return (
        <div  className="data-table mt-5 align-self-center">
            <div className="row">
                <div className="col-lg">
                    <div className="icon-mission align-items-center">
                        <img src={IconListMission} alt="icon" className="icon-img" />
                        <span className="text-app">User's App Check In </span>
                    </div>
                </div>
                <div className="col-lg head-date text-end d-flex justify-content-end">
                    <div className="date-container">
                        <input type="date" value={firstDate} className="date-table date-table-left" onChange={handleChangeFirstDate} />
                        <span className="text-to">To</span>
                        <input type="date" value={secondDate} className="date-table date-table-right" onChange={handleChangeSecondDate} />
                    </div>
                </div>
            </div>
            {checkinloading && 
                <p>
                    <Spinner color="primary" children="" className="mt-3"/>
                </p>
            }
            {!checkinloading && checkinData &&
                <div id="checkin" ref={myRef} className="table-checkin mt-4 mb-5">
                <ReactDatatable
                    config={config}
                    records={checkinData}
                    columns={columns} 
                    
                    />
                    
                </div>
            }
            
        </div>
    )
}

export default TableUserCheckin
