import './tableMissionCompletion.scss';
import { useState, useEffect } from 'react';
import ReactDatatable from '@ashvin27/react-datatable';
import IconListMission from '../../../../asset/img/icon_list_mission.png'
import { dateNow } from '../../../../services/helpers';
import { useDispatch, useSelector } from 'react-redux'
import { tableMissionCompletion } from "../../../../redux/point-action"
import { Spinner } from 'reactstrap';

const TableMissionCompletion = (props) => {
    const dispatch = useDispatch()
    const [firstDate, setFirstDate] = useState(dateNow())
    const [secondDate, setSecondDate] = useState(dateNow())
    const missionloading = useSelector(state => state.isLoading3)
    const missionData = useSelector(state => state.tableMission)
    useEffect(() => {
        dispatch(tableMissionCompletion())
    }, [])
    const columns = [
        {
            key: "cid",
            text: "CID",
            sortable: true
        },
        {
            key: "fullName",
            text: "FULL NAME",
            sortable: true
        },
        {
            key: "missionTransaction",
            text: "MISSION TRANSACTIONS",
            sortable: true
        },
        {
            key: "pointsUnlocked",
            text: "POINTS UNLOCKED",
            sortable: true
        },
        {
            key: "date",
            text: "DATE",
            sortable: true
        },
        {
            key: "timeMark",
            text: "TIME MARK",
            sortable: true
        },
        {
            key: "status",
            text: "STATUS",
            cell: (record, index) => {

                return (
                    record.status ?
                        <div>
                            <div className="status-active " >
                                <span className="text-active">Completed</span>
                            </div>
                        </div> :
                        !record.status ?
                            <div>
                                <div className="status-inactive" >
                                    <span className="text-inactive">Inactive</span>
                                </div>
                            </div> : '')
            }
        }
    ]

    const config = {
        page_size: 10,
        length_menu: [10, 20, 50],
        show_filter: true,
        show_pagination: true,
        pagination: 'basic',
        button: {
            csv: true
        }
    }

    const handleChangeFirstDate = (e) => {
        const startDate = (secondDate < e.target.value) ?
            secondDate : e.target.value
        setFirstDate(startDate)
        let date = {
            start: startDate,
            end: secondDate
        }
        dispatch(tableMissionCompletion(date))
    }

    const handleChangeSecondDate = (e) => {
        const endDate = (firstDate > e.target.value) ?
            firstDate : e.target.value
        setSecondDate(endDate)
        let date = {
            start: firstDate,
            end: endDate
        }
        dispatch(tableMissionCompletion(date))
    }

    return (
        <div className="data-table mt-5 align-self-center">
            <div className="row">
                <div className="col-lg">
                    <div className="icon-mission align-items-center">
                        <img src={IconListMission} alt="icon" className="icon-img" />
                        <span className="text-app">User's Mission Completion </span>
                    </div>
                </div>
                <div className="col-lg head-date text-end d-flex justify-content-end">
                    <div className="date-container">
                        <input type="date" value={firstDate} className="date-table date-table-left" onChange={handleChangeFirstDate} />
                        <span className="text-to">To</span>
                        <input type="date" value={secondDate} className="date-table date-table-right" onChange={handleChangeSecondDate} />
                    </div>
                </div>
            </div>
            {missionloading &&
                <p>
                    <Spinner color="primary" children="" className="mt-3" />
                </p>
            }
            {!missionloading && missionData &&
                <div className="table-checkin mt-4 mb-5">
                    <ReactDatatable
                        config={config}
                        records={missionData}
                        columns={columns} />
                </div>
            }
        </div>
    )

}

export default TableMissionCompletion