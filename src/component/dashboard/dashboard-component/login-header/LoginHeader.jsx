import { Navbar, NavbarBrand, Container } from 'reactstrap';
import LogoSVG from "../../../../asset/img/header_logo.svg"
import "./loginheader.scss"

const LoginHeader = () => {
  return (
    <div className="login-nav-border-bottom">
      <Container>
        <Navbar color="white" light expand="md">
          <NavbarBrand href="/"><img src={LogoSVG} alt="Logo" /></NavbarBrand>
        </Navbar>
        </Container>
    </div>
  )
}

export default LoginHeader