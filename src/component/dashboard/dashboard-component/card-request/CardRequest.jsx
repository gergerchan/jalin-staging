import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import { IoSearchCircle } from "react-icons/io5";
import newCustomer from '../../../../asset/img/new-customer-icon.svg';
import profileUpdate from '../../../../asset/img/profile-update-icon.svg';
import newTickets from '../../../../asset/img/new-tickets-icon.svg';
import accountDeletion from '../../../../asset/img/account-deletion-icon.svg';
import './cardRequest.scss';

const CardRequest = () => {

    const [newCustomerModal, setNewCustomerModal] = useState(false);
    const [updateProfileModal, setUpdateProfileModal] = useState(false);
    const [newTicketsModal, setNewTicketsModal] = useState(false);
    const [deleteReqModal, setDeleteReqModal] = useState(false);


    const toggleNewCustomerModal = () => setNewCustomerModal(!newCustomerModal);
    const toggleUpdateProfileModal = () => setUpdateProfileModal(!updateProfileModal);
    const toggleNewTicketsModal = () => setNewTicketsModal(!newTicketsModal);
    const toggleDeleteReqModal = () => setDeleteReqModal(!deleteReqModal);

    const closeNewCustomerBtn = <button className="btn-close" onClick={toggleNewCustomerModal}></button>;
    const closeUpdateProfileBtn = <button className="btn-close" onClick={toggleUpdateProfileModal}></button>;
    const closeNewTicketsBtn = <button className="btn-close" onClick={toggleNewTicketsModal}></button>;
    const closeDeleteReqBtn = <button className="btn-close" onClick={toggleDeleteReqModal}></button>;

    return (
        <div className='base home-card-request'>
            <div className="card-request">
                <p className='card-request-heading'>Requests</p>
                <ul className='card-request-list'>
                    <li className='row card-request-list-row'>
                        <Link
                            className='col-9 card-request-list-item'
                            onClick={toggleNewCustomerModal}>
                            <img className='card-request-list-icon' src={newCustomer} alt="new customer icon" /> New Customer
                        </Link>
                        <p className="col-3 card-request-list-data-item">20</p>
                    </li>
                    <li className='row card-request-list-row border border-start-0 border-end-0'>
                        <Link
                            className='col-9 card-request-list-item'
                            onClick={toggleUpdateProfileModal}>
                            <img className='card-request-list-icon' src={profileUpdate} alt="profile update icon" /> Profile Update
                        </Link>
                        <p className="col-3 card-request-list-data-item">5</p>
                    </li>
                    <li className='row card-request-list-row border border-top-0 border-start-0 border-end-0'>
                        <Link
                            className='col-9 card-request-list-item'
                            onClick={toggleNewTicketsModal}>
                            <img className='card-request-list-icon ' src={newTickets} alt="new support tickets" /> New Support Tickets
                        </Link>
                        <p className="col-3 card-request-list-data-item">16</p>
                    </li>
                    <li className='row card-request-list-row'>
                        <Link
                            className='col-9 card-request-list-item'
                            onClick={toggleDeleteReqModal}>
                            <img className='card-request-list-icon' src={accountDeletion} alt="account deletion" /> Account Deletion
                        </Link>
                        <p className="col-3 card-request-list-data-item">2</p>
                    </li>
                </ul>
            </div>

            <Modal className='base new-customer-modal' size='lg' isOpen={newCustomerModal} toggle={toggleNewCustomerModal}>
                <ModalHeader className='new-customer-modal-heading border-bottom-0' toggle={toggleNewCustomerModal} close={closeNewCustomerBtn}>
                    <img className='new-customer-icon' src={newCustomer} alt="new customer icon" />
                    <span className='new-customer-modal-title'>New Customer Request</span>
                </ModalHeader>
                <ModalBody>
                    <Table borderless>
                        <thead className='border border-top-0 border-start-0 border-end-0'>
                            <tr className='new-customer-table-heading'>
                                <th style={{ width: '260px' }}>REQUEST NAME</th>
                                <th style={{ width: '270px' }}>EMAIL</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Nabella Khorinnissa</td>
                                <td>nabellak0000@gmail.com</td>
                                <td className='d-flex justify-content-start'>
                                    <div className="row">
                                        <div className="col-6 btn-action-box"><button className='btn-accept'>Accept</button></div>
                                        <div className="col-6 btn-action-box"><button className='btn-details'>View Details</button></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Afrida Restu Suryani</td>
                                <td>afridarestu@hotmail.com</td>
                                <td className='d-flex justify-content-start'>
                                    <div className="row">
                                        <div className="col-6 btn-action-box"><button className='btn-accept'>Accept</button></div>
                                        <div className="col-6 btn-action-box"><button className='btn-details'>View Details</button></div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </ModalBody>
            </Modal>

            <Modal className='base new-customer-modal' size='md' isOpen={updateProfileModal} toggle={toggleUpdateProfileModal}>
                <ModalHeader className='new-customer-modal-heading border-bottom-0' toggle={toggleUpdateProfileModal} close={closeUpdateProfileBtn}>
                    <img className='new-customer-icon' src={profileUpdate} alt="Profile Update Request" />
                    <span className='new-customer-modal-title'>Profile Update Request</span>
                </ModalHeader>
                <ModalBody>
                    <Table borderless>
                        <thead className='border border-top-0 border-start-0 border-end-0'>
                            <tr className='new-customer-table-heading'>
                                <th style={{ width: '260px' }}>CUSTOMER NAME</th>
                                <th style={{ width: '270px' }}>CHANGES MADE</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Pradipta Eva Trudiyanti</td>
                                <td>ID Card</td>
                                <td className='d-flex justify-content-start'>
                                    <button className='btn-changes'><IoSearchCircle color='#287CA1' size='30px' /> View changes</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Christine</td>
                                <td>Phone number</td>
                                <td className='d-flex justify-content-start'>
                                    <button className='btn-changes'><IoSearchCircle color='#287CA1' size='30px' /> View changes</button>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </ModalBody>
            </Modal>

            <Modal className='base new-customer-modal' size='xl' isOpen={newTicketsModal} toggle={toggleNewTicketsModal}>
                <ModalHeader className='new-customer-modal-heading border-bottom-0' toggle={toggleNewTicketsModal} close={closeNewTicketsBtn}>
                    <img className='new-customer-icon' src={newTickets} alt="new support tickets" />
                    <span className='new-customer-modal-title'>New Support Tickets</span>
                </ModalHeader>
                <ModalBody>
                    <Table borderless>
                        <thead className='border border-top-0 border-start-0 border-end-0'>
                            <tr className='new-customer-table-heading'>
                                <th style={{ width: '210px' }}>TICKET NUMBER</th>
                                <th style={{ width: '210px' }}>TICKET SUBJECT</th>
                                <th style={{ width: '240px' }}>REQUESTER NAME</th>
                                <th style={{ width: '210px' }}>REQUESTER ON</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>#JALIN-013</td>
                                <td>Register problem</td>
                                <td>Daniel Agustian</td>
                                <td>31-05-2021</td>
                                <td className='d-flex justify-content-start'>
                                    <div className="row">
                                        <div className="col-6 btn-action-box"><button className='btn-accept'>Reply</button></div>
                                        <div className="col-6 btn-action-box"><button className='btn-skip'>Skip</button></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>#JALIN-012</td>
                                <td>Transfer failed</td>
                                <td>Dimas Trivianto</td>
                                <td>06-06-2021</td>
                                <td className='d-flex justify-content-start'>
                                    <div className="row">
                                        <div className="col-6 btn-action-box"><button className='btn-accept'>Reply</button></div>
                                        <div className="col-6 btn-action-box"><button className='btn-skip'>Skip</button></div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </ModalBody>
            </Modal>

            <Modal className='base new-customer-modal' size='xl' isOpen={deleteReqModal} toggle={toggleDeleteReqModal}>
                <ModalHeader className='new-customer-modal-heading border-bottom-0' toggle={toggleDeleteReqModal} close={closeDeleteReqBtn}>
                    <img className='new-customer-icon' src={accountDeletion} alt="account deletion request" />
                    <span className='new-customer-modal-title'>Account Deletion Request</span>
                </ModalHeader>
                <ModalBody>
                    <Table borderless>
                        <thead className='border border-top-0 border-start-0 border-end-0'>
                            <tr className='new-customer-table-heading'>
                                <th style={{ width: '240px' }}>REQUESTER NAME</th>
                                <th style={{ width: '270px' }}>REQUESTER EMAIL</th>
                                <th style={{ width: '360px' }}>REASON</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Kuroba Kaito</td>
                                <td>kurobakaito@gmail.com</td>
                                <td>Move to other bank account</td>
                                <td className='d-flex justify-content-start'>
                                    <div className="row">
                                        <div className="col-6 btn-action-box"><button className='btn-accept'>Accept</button></div>
                                        <div className="col-6 btn-action-box"><button className='btn-details'>View Details</button></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Akashi Seijuurou</td>
                                <td>akashiseijuurou@hotmail.com</td>
                                <td>The services not relevant for me anymore</td>
                                <td className='d-flex justify-content-start'>
                                    <div className="row">
                                        <div className="col-6 btn-action-box"><button className='btn-accept'>Accept</button></div>
                                        <div className="col-6 btn-action-box"><button className='btn-details'>View Details</button></div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </ModalBody>
            </Modal>
        </div>
    )
}

export default CardRequest