import { useState } from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { FiSettings, FiLogOut } from "react-icons/fi";
import fotoProfile from '../../../../asset/img/foto-profile.svg'
import './profile.scss'
import Cookies from 'js-cookie';
import { useHistory } from 'react-router-dom';


const Profile = () => {
    let history = useHistory()
    const [dropdownOpen, setOpen] = useState(false);

    const toggle = () => setOpen(!dropdownOpen);

    const handleLogout = () => {
        Cookies.remove("TOKEN_USER");
        Cookies.remove("EMAIL_USER");
        history.push("/");
    }

    return (
        <div className="profile">
                <img className="foto-profile" src={fotoProfile} alt="fotoProfile" />
                <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
                    <DropdownToggle caret color="white">
                        ADMINISTRATOR
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem disabled>Administrator</DropdownItem>
                        <DropdownItem disabled>{Cookies.get("EMAIL_USER")}</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem onClick={handleLogout}> <FiLogOut />Log Out</DropdownItem>
                    </DropdownMenu>
                </ButtonDropdown>
        </div>
    );

}

export default Profile
