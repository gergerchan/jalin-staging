import { Calendar, momentLocalizer } from 'react-big-calendar'
import 'react-big-calendar/lib/css/react-big-calendar.css';
import moment from 'moment'
import "./calendar.scss"
// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
const localizer = momentLocalizer(moment) // or globalizeLocalizer

const eventStyleGetter = (event, start, end, isSelected) => {
    // console.log(event);
    var backgroundColor = event.hexColor;
    var style = {
        backgroundColor: backgroundColor,
        borderRadius: '0px',
        opacity: 0.8,
        color: 'white',
        border: '0px',
        display: 'block'
    };
    return {
        style: style
    };
}

const Calendardashboard = (props) => {
    return (
        <div>
            <Calendar
                localizer={localizer}
                events={props.data}
                startAccessor="start"
                endAccessor="end"
                view='month'
                views={['month']}
                style={{ height: 500, width: '95%' }}
                eventPropGetter={(eventStyleGetter)}
            />
            <div className="calendar-detail text-center mt-3">
                <div className="row">
                    <div className="col-lg-3"></div>
                    <div className="col-lg-3 d-flex justify-content-start"><span className="align-middle green" />Checkin</div>
                    <div className="col-lg-5 d-flex justify-content-start"><span className="align-middle red" />Mission Complete</div>
                    <div className="col-lg-1"></div>
                </div>
            </div>
        </div>
    )
}

export default Calendardashboard
