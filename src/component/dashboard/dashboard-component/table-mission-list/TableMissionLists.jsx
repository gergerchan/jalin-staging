import React, { useState } from 'react';
import './tableMissionLists.scss';
import ReactDatatable from '@ashvin27/react-datatable';
import IconListMission from '../../../../asset/img/icon_list_mission.png'
import { FaSearch, FaPencilAlt, FaTrashAlt, FaPlusCircle } from "react-icons/fa"
import PointsRewardsModals from '../points-rewards-modals/PointsRewardsModals';
import detailMission from "../../../../asset/img/mission-detail-icon.svg";
import addMission from "../../../../asset/img/add-mission-list-icon.svg";
import DeleteModals from '../points-rewards-modals/DeleteModals';
import EditModals from '../points-rewards-modals/EditModals';
import DetailMissionModals from '../points-rewards-modals/DetailMissionModals';

const TableMissionList = (props) => {
    // state buat nge trigger modalsnya
    const [rewardsModal, setRewardsModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const [detailMissionModal, setDetailMissionModal] = useState(false);
    const [data, setData] = useState(null)

    const toggleRewardsModal = () => setRewardsModal(!rewardsModal);
    const toggleDeleteModal = () => setDeleteModal(!deleteModal);
    const toggleEditModal = () => setEditModal(!editModal);
    const toggleDetailMissionModal = () => setDetailMissionModal(!detailMissionModal);

    const closeRewardsBtn = <button className="btn-close" onClick={toggleRewardsModal}></button>;
    const closeEditBtn = <button className="btn-close" onClick={toggleEditModal}></button>;
    const closeDetailMissionBtn = <button className="btn-close" onClick={toggleDetailMissionModal}></button>;

    const columns = [
        {
            key: "number",
            text: "#",
            sortable: true
        },
        {
            key: "activity",
            text: "MISSION",
            sortable: true
        },
        {
            key: "frequency",
            text: "FREQUENCY",
            sortable: true
        },
        {
            key: "point",
            text: "POINTS",
            sortable: true
        },
        {
            key: "expiration",
            text: "USER ROLE",
            sortable: true
        },
        {
            key: "status",
            text: "STATUS",
            cell: (record, index) => {
                return (
                    record.status ?
                        <div>
                            <div className="status-active" >
                                <span className="text-active">Active</span>
                            </div>
                        </div> :
                        !record.status ?
                            <div>
                                <div className="status-inactive" >
                                    <span className="text-inactive">Inactive</span>
                                </div>
                            </div> : '')
            }
        },
        {
            key: "action",
            text: "ACTION",
            width: 120,
            cell: (record, index) => {
                return (
                    <div className="action">
                        <div className="action-button">
                            <button onClick={toggleDetailMissionModal} className="btn btn-primary btn-sm btn-action-search">
                                <div className="search">
                                    <FaSearch size="11px" className="icon-search" />
                                </div>
                            </button>
                            <button onClick={toggleEditModal} className="btn btn-primary btn-sm btn-action-edit">
                                <div className="edit">
                                    <FaPencilAlt size="11px" className="icon-edit" />
                                </div>
                            </button>
                            <button onClick={toggleDeleteModal} className="btn btn-primary btn-sm btn-action-delete">
                                <div className="delete">
                                    <FaTrashAlt size="11px" className="icon-delete" />
                                </div>
                            </button>
                        </div>
                    </div>
                )
            }
        },
    ]

    const config = {
        page_size: 5,
        length_menu: [5, 10, 20, 50],
        show_filter: true,
        show_pagination: true,
        pagination: 'basic',
        button: {
            csv: false
        }
    }

    const rowClickedHandler = (event, data, rowIndex) => {
        setData(data);
    }

    return (
        <div className="data-table mt-5 align-self-center">
            <div className="row ">
                <div className="col-lg">
                    <div className="icon-mission align-items-center">
                        <img src={IconListMission} alt="icon" className="icon-img" />
                        <span className="text-app">Mission List</span>
                    </div>
                </div>
                <div className="col-lg text-end button-add-">
                    <button onClick={toggleRewardsModal} className="add- px-3 py-2 mx-3">Add Mission <FaPlusCircle className="icon-plus mb-1 align-self-center" /> </button>
                </div>
            </div>
            <div className="table-checkin mt-4 mb-5">
                <ReactDatatable
                    config={config}
                    records={props.data}
                    columns={columns}
                    onRowClicked={rowClickedHandler}
                />
            </div>

            <PointsRewardsModals
                isOpen={rewardsModal}
                toggle={toggleRewardsModal}
                closeBtn={closeRewardsBtn}
                modalIcon={addMission}
                modalTitle={'Add Mission List'}
                toggleDesc={'mission'}
                formLabel={'Mission List'}
            />

            <EditModals
                isOpen={editModal}
                toggle={toggleEditModal}
                closeBtn={closeEditBtn}
                modalIcon={addMission}
                modalTitle={'Edit Mission List'}
                formLabel={'Mission List'}
                data={data}
            />

            <DetailMissionModals
                isOpen={detailMissionModal}
                toggle={toggleDetailMissionModal}
                closeBtn={closeDetailMissionBtn}
                modalIcon={detailMission}
                modalTitle={'Mission Details'}
                formLabel={'Mission List'}
                data={data}
            />

            <DeleteModals
                isOpen={deleteModal}
                toggle={toggleDeleteModal}
                toggleDesc={'mission'}
                data={data}
                mission={true}
            />
        </div>
    )

}

export default TableMissionList