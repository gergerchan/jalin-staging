import "./card-info.scss"
const CardInfo = ({ title, number, number2, icon }) => {

    return (
        <div className="cardCustomer p-xl-4 p-sm-4">
            <div className="row ">
                <div className="col-9 ps-xl-4 ps-sm-2 ps-sm-0">
                    <p className="b-700 m-0">{title}</p>
                    <p className="b-700 m-0"> <span className="number"> {number} </span> {number2 && <span className="percentage ms-3"> {number2}  </span>}</p>
                </div>
                <div className="col-3 pe-xl-4 pe-sm-0">
                    <div className="d-flex justify-content-end">
                        <img className="cardCustomer-img" src={icon} alt="" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardInfo;