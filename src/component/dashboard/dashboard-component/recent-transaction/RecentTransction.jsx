import { Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import './recentTransaction.scss'

const RecentTransaction = (props) => {
    return (
        <div className='base home-user-app-recent-transaction my-5'>
            <div className="row home-user-app-recent-transaction-heading">
                <h5 className="col-8 home-user-app-recent-transaction-title">Recent Transaction</h5>
                <Link to="/transaction" className="col-4 home-user-app-recent-transaction-viewall">View all</Link>
            </div>
            <div className='user-recent-transaction-table'>
                <Table borderless>
                    <thead className='user-recent-transaction-table-heading'>
                        <tr>
                            <th className='user-transaction-left-heading ps-4 ps-lg-4 ps-sm-2 py-3'>ID</th>
                            <th className='user-transaction-mid-left-heading ps-4 ps-sm-2 py-3'>Transaction</th>
                            <th className='user-transaction-mid-heading ps-4 ps-sm-2 py-3'>To</th>
                            <th className='user-transaction-mid-right-heading ps-4 ps-sm-2 py-3'>Time</th>
                            <th className='user-transaction-right-heading ps-4 ps-sm-2 py-3'>Cash Flow</th>
                        </tr>
                    </thead>
                    <tbody className='user-recent-transaction-table-items'>
                        {props.data.map((data, i) =>
                            <tr>
                                <td className='user-recent-transaction-row ps-4 ps-lg-4 ps-sm-2 py-3'>{data.id}</td>
                                <td className='user-recent-transaction-row ps-4 ps-sm-2 py-3'>{data.Transaction}</td>
                                <td className='user-recent-transaction-row ps-4 ps-sm-2 py-3'>{data.Destination}</td>
                                <td className='user-recent-transaction-row ps-4 ps-sm-2 py-3'>{data.Time}</td>
                                <td className='user-recent-transaction-row ps-4 ps-sm-2 py-3'>{data.CashFlow}</td>
                            </tr>
                        )}
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default RecentTransaction