import React, { useState } from 'react';
import './tablePromoLists.scss';
import ReactDatatable from '@ashvin27/react-datatable';
import IconVoucherList from '../../../../asset/img/icon_voucher_list.png'
import { FaSearch, FaPencilAlt, FaTrashAlt, FaPlusCircle } from "react-icons/fa"
import PointsRewardsModals from '../points-rewards-modals/PointsRewardsModals';
import addMission from "../../../../asset/img/add-mission-list-icon.svg";
import DeleteModals from '../points-rewards-modals/DeleteModals';
import EditModals from '../points-rewards-modals/EditModals';

const TableVoucherList = () => {
    // state buat nge trigger modalsnya
    const [rewardsModal, setRewardsModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const [detailModal, setDetailModal] = useState(false);

    const toggleRewardsModal = () => setRewardsModal(!rewardsModal);
    const toggleDeleteModal = () => setDeleteModal(!deleteModal);
    const toggleEditModal = () => setEditModal(!editModal);
    const toggleDetailModal = () => setDetailModal(!detailModal);

    const closeRewardsBtn = <button className="btn-close" onClick={toggleRewardsModal}></button>;
    const closeEditBtn = <button className="btn-close" onClick={toggleEditModal}></button>;
    const closeDetailBtn = <button className="btn-close" onClick={toggleDetailModal}></button>;

    const columns = [
        {
            key: "number",
            text: "#",
            sortable: false
        },
        {
            key: "code",
            text: "CODE",
            sortable: true
        },
        {
            key: "promo",
            text: "PROMO",
            sortable: true
        },
        {
            key: "amount",
            text: "AMOUNT",
            sortable: true
        },
        {
            key: "points",
            text: "POINTS",
            sortable: true
        },
        {
            key: "user_role",
            text: "EXPIRED IN",
            cell: (record, index) => {
                return (
                    <div className="user-role">
                        <span className="week-role" >1 Years</span>
                    </div>
                )
            }
        },
        {
            key: "status",
            text: "STATUS",
            cell: (record, index) => {
                return (
                    record.status ?
                        <div>
                            <div className="status-active text-center" >
                                <span className="text-active">Active</span>
                            </div>
                        </div> :
                        !record.status ?
                            <div>
                                <div className="status-inactive text-center" >
                                    <span className="text-inactive">Inactive</span>
                                </div>
                            </div> : '')
            }
        },
        {
            key: "action",
            text: "ACTION",
            TrOnlyClassName: "action-table",
            cell: (record, index) => {
                return (
                    <div className="action">
                        <div className="action-button">
                            <button onClick={toggleDetailModal} className="btn btn-primary btn-sm btn-action-search">
                                <div className="search">
                                    <FaSearch size="11px" className="icon-search" />
                                </div>
                            </button>
                            <button onClick={toggleEditModal} className="btn btn-primary btn-sm btn-action-edit">
                                <div className="edit">
                                    <FaPencilAlt size="11px" className="icon-edit" />
                                </div>
                            </button>
                            <button onClick={toggleDeleteModal} className="btn btn-primary btn-sm btn-action-delete">
                                <div className="delete">
                                    <FaTrashAlt size="11px" className="icon-delete" />
                                </div>
                            </button>
                        </div>
                    </div>
                )
            }
        },
    ]

    const config = {
        page_size: 10,
        length_menu: [10, 20, 50],
        show_filter: true,
        show_pagination: true,
        pagination: 'basic',
        button: {
            csv: true
        }
    }
    const rowClickedHandler = (event, data, rowIndex) => {
        console.log("event", event);
        console.log("row data", data);
        console.log("row index", rowIndex);
    }

    return (
        <div className="data-table mt-5 align-self-center">
            <div className="row">
                <div className="col-lg">
                    <div className="icon-mission align-items-center">
                        <img src={IconVoucherList} alt="icon" className="icon-img" />
                        <span className="text-app">Promo List</span>
                    </div>
                </div>
                <div className="col-lg text-end button-add-">
                    <button onClick={toggleRewardsModal} className="add- px-3 py-2 mx-3">Add Promo<FaPlusCircle className="icon-plus mb-1 align-self-center" /> </button>
                </div>
            </div>
            <div className="table-checkin mt-4 mb-5">
                <ReactDatatable
                    config={config}
                    // records={records}
                    onRowClicked={rowClickedHandler}
                    columns={columns} />
            </div>

            <PointsRewardsModals 
                isOpen={rewardsModal}
                toggle={toggleRewardsModal} 
                closeBtn={closeRewardsBtn}
                modalIcon={addMission}
                modalTitle={'Add Promo List'}
                toggleDesc={'promo'}
                formLabel={'Promo List'}
            />

            <EditModals 
                isOpen={editModal}
                toggle={toggleEditModal} 
                closeBtn={closeEditBtn}
                modalIcon={addMission}
                modalTitle={'Edit Promo List'}
                formLabel={'Promo List'}
            />

            {/* <DetailModals 
                isOpen={detailModal}
                toggle={toggleDetailModal} 
                closeBtn={closeDetailBtn}
                modalIcon={addMission}
                modalTitle={'View Detail Promo'}
                formLabel={'Promo List'}
            /> */}

            <DeleteModals 
                isOpen={deleteModal}
                toggle={toggleDeleteModal} 
                toggleDesc={'promo'}
            />
        </div>
    )

}

export default TableVoucherList