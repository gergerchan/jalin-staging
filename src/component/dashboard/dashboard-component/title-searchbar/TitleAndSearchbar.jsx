// import { FaSearch } from "react-icons/fa"
import './titleSearchbar.scss'

const TitleAndSearchbar = (props) => {
    return (
        <div className="row titleandsearchbar mt-3 pb-4">
            <div className="col d-flex align-items-center">
                <h3 className="b-700">{props.title}</h3>
            </div>
            {/* <div class="searchContainer">
                <i class="searchIcon"><FaSearch /></i>
                <input className="searchBox" type="search" name="search" placeholder="Search..."></input>
            </div> */}
        </div>
    )
}

export default TitleAndSearchbar
