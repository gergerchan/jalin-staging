import { NavLink } from 'react-router-dom';
import Jalin from '../../../../asset/img/logo_jalin 1.svg';
import DashboardIcon from '../../../../asset/img/dashboard-icon.svg';
import CustomerIcon from '../../../../asset/img/customer-icon.svg';
import SavingsIcon from '../../../../asset/img/savings-icon.svg';
import TrophyIcon from '../../../../asset/img/trophy-icon.svg';
import LeaderBoardIcon from '../../../../asset/img/leaderboard-icon.svg';
import SettingsIcon from '../../../../asset/img/settings-icon.svg';
import SupportIcon from '../../../../asset/img/support-icon.svg';
import './sidebar.scss';

const Sidebar = () => {
    return (
        <>
            <div className="sidebar pt-3 px-xl-3 px-2 sticky-top">
                <div className="row">
                    <div className="col d-flex justify-content-center"><img src={Jalin} alt="jalin-logo"/></div>
                </div>
                <div className="row mt-5">
                    <div className="col-12 ">
                        <h4 className='sidebar-nav-heading b-700 px-2 mb-3'>General</h4>
                    </div>
                    <div className="col-12 ">
                        <NavLink to="/dashboard" className='sidebar-nav-item d-flex align-items-center py-2 ps-xl-2 ps-1 mb-1' activeClassName="nav-item-active ">
                            <img className='sidebar-nav-icon mx-3 mx-sm-2 ms-sm-1' src={DashboardIcon} alt="DashboardIcon" /> <span>Dashboard</span>
                        </NavLink>
                    </div>
                    <div className="col-12">
                        <NavLink to="/points-and-rewards" className='sidebar-nav-item d-flex align-items-center py-2 ps-xl-2 ps-1 mb-1' activeClassName="nav-item-active">
                            <img className='sidebar-nav-icon mx-3 mx-sm-2 ms-sm-1' src={TrophyIcon} alt="TrophyIcon" /> Points & Rewards
                        </NavLink>
                    </div>
                    <div className="col-12">
                        <NavLink to="/leaderboard" className='sidebar-nav-item d-flex align-items-center py-2 ps-xl23 ps-1 mb-1' activeClassName="nav-item-active">
                            <img className='sidebar-nav-icon mx-3 mx-sm-2 ms-sm-1' src={LeaderBoardIcon} alt="LeaderboardIcon" />Leaderboard
                        </NavLink>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Sidebar
