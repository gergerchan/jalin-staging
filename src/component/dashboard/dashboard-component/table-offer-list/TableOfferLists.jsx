import React, { useState } from 'react';
import './tableOfferLists.scss';
import ReactDatatable from '@ashvin27/react-datatable';
import IconOfferList from '../../../../asset/img/icon_offers_list.png';
import { FaSearch, FaPencilAlt, FaTrashAlt, FaPlusCircle } from "react-icons/fa";
import detailMission from "../../../../asset/img/mission-detail-icon.svg";
import addMission from "../../../../asset/img/add-mission-list-icon.svg";
import DeleteModals from '../points-rewards-modals/DeleteModals';
import DetailOfferVoucherModals from '../points-rewards-modals/DetailOfferVoucherModals';
import AddOfferVoucherModals from '../points-rewards-modals/AddOfferVoucherModals';
import EditOfferVoucherModals from '../points-rewards-modals/EditOfferVoucherModals';

const TableOfferList = (props) => {

    const [dataOffer, setDataOffer] = useState(null)
    // state buat nge trigger modalsnya
    const [addOfferVoucherModal, setAddOfferVoucherModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [editOfferVoucherModal, setEditOfferVoucherModal] = useState(false);
    const [detailOfferVoucherModal, setDetailOfferVoucherModal] = useState(false);

    const toggleAddOfferVoucherModal = () => setAddOfferVoucherModal(!addOfferVoucherModal);
    const toggleDeleteModal = () => setDeleteModal(!deleteModal);
    const toggleEditOfferVoucherModal = () => setEditOfferVoucherModal(!editOfferVoucherModal);
    const toggleDetailOfferVoucherModal = () => setDetailOfferVoucherModal(!detailOfferVoucherModal);

    const closeAddOfferVoucherBtn = <button className="btn-close" onClick={toggleAddOfferVoucherModal}></button>;
    const closeEditOfferVoucherBtn = <button className="btn-close" onClick={toggleEditOfferVoucherModal}></button>;
    const closeDetailOfferVoucherBtn = <button className="btn-close" onClick={toggleDetailOfferVoucherModal}></button>;

    const columns = [
        {
            key: "number",
            text: "#",
            sortable: false,
            width: "30px"
        },
        {
            key: "title",
            text: "OFFERS",
            sortable: true,
            width: "250px"
        },
        {
            key: "tncDescription",
            text: "USED FOR",
            sortable: true,
            width: "250px"
        },
        {
            key: "quota",
            text: "FREQUENCY",
            sortable: true,

        },
        {
            key: "points",
            text: "POINTS",
            sortable: true
        },
        {
            key: "validity",
            text: "EXPIRED IN",
            sortable: true,
            width: "150"
        },
        {
            key: "status",
            text: "STATUS",
            width: "100",
            cell: (record, index) => {
                return (
                    record.status ?
                        <div>
                            <div className="status-active" >
                                <span className="text-active">Active</span>
                            </div>
                        </div> :
                        !record.status ?
                            <div>
                                <div className="status-inactive" >
                                    <span className="text-inactive">Inactive</span>
                                </div>
                            </div> : '')
            }
        },
        {
            key: "action",
            text: "ACTION",
            width: "150",
            cell: (record, index) => {
                return (
                    <div className="action">
                        <div className="action-button">
                            <button onClick={toggleDetailOfferVoucherModal} className="btn btn-primary btn-sm btn-action-search">
                                <div className="search">
                                    <FaSearch size="11px" className="icon-search" />
                                </div>
                            </button>
                            <button onClick={toggleEditOfferVoucherModal} className="btn btn-primary btn-sm btn-action-edit">
                                <div className="edit">
                                    <FaPencilAlt size="11px" className="icon-edit" />
                                </div>
                            </button>
                            <button onClick={toggleDeleteModal} className="btn btn-primary btn-sm btn-action-delete">
                                <div className="delete">
                                    <FaTrashAlt size="11px" className="icon-delete" />
                                </div>
                            </button>
                        </div>
                    </div>
                )
            }
        },
    ]

    const config = {
        page_size: 5,
        length_menu: [5, 10, 20, 50],
        show_filter: true,
        show_pagination: true,
        pagination: 'basic'
    }

    const rowClickedHandler = (event, data, rowIndex) => {
        setDataOffer(data);
    }

    return (
        <div className="data-table mt-5 align-self-center">
            <div className="row">
                <div className="col-lg">
                    <div className="icon-mission align-items-center">
                        <img src={IconOfferList} alt="icon" className="icon-img" />
                        <span className="text-app">Jalin Offer List</span>
                    </div>
                </div>
                <div className="col-lg text-end button-add-">
                    <button onClick={toggleAddOfferVoucherModal} className="add- px-3 py-2 mx-3">Add Offers<FaPlusCircle className="icon-plus mb-1 align-self-center" /> </button>
                </div>
            </div>
            <div className="table-checkin mt-4 mb-5">
                <ReactDatatable
                    config={config}
                    records={props.data}
                    columns={columns}
                    onRowClicked={rowClickedHandler}
                />
            </div>

            <AddOfferVoucherModals
                isOpen={addOfferVoucherModal}
                toggle={toggleAddOfferVoucherModal}
                closeBtn={closeAddOfferVoucherBtn}
                modalIcon={addMission}
                modalTitle={'Add Offer List'}
                toggleDesc={'Offer'}
                formLabel={{
                    label1: 'Offer List',
                    label2: 'Frequency'
                }}
                placeholder={'Frequency'}
                voucher={false}
            />

            <EditOfferVoucherModals
                isOpen={editOfferVoucherModal}
                toggle={toggleEditOfferVoucherModal}
                closeBtn={closeEditOfferVoucherBtn}
                modalIcon={addMission}
                modalTitle={'Edit Offer List'}
                formLabel={{
                    label1: 'Offer List',
                    label2: 'Frequency'
                }}
                data={dataOffer}
                placeholder={'Frequency'}
            />

            <DetailOfferVoucherModals
                isOpen={detailOfferVoucherModal}
                toggle={toggleDetailOfferVoucherModal}
                closeBtn={closeDetailOfferVoucherBtn}
                modalIcon={detailMission}
                modalTitle={'Offer Details'}
                formLabel={{
                    label1: 'Offer List',
                    label2: 'Expired In',
                    label3: 'Points',
                    label4: 'Frequency'
                }}
                description={'Free Admin Fee for Top Up ShopeePay'}
                dataOffer={dataOffer}
            />

            <DeleteModals
                isOpen={deleteModal}
                toggle={toggleDeleteModal}
                toggleDesc={'offer'}
                Offer={true}
                data={dataOffer}
            />
        </div>
    )

}

export default TableOfferList