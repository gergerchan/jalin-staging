import "./TransactionCard.scss"

const TransactionCard = (props) => {
    return (
        <>
            <div className="transaction-card d-flex justify-content-between p-3 mt-4">
                <span className="transaction-card-title">{props.title}</span>
                <div className={`transaction-card-circle ${props.color} b-700 p-3`}>
                    {props.number}
                </div>
            </div>
        </>
    )
}

export default TransactionCard
