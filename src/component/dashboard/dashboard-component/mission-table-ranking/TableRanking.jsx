import { Table } from 'reactstrap';
import "./tableRanking.scss";
import rank4 from "../../../../asset/img/rank4.svg";

const TableRanking = (props) => {
    let string
    const data = props.data.map((data,i) => {
        string = data.jalinId.split(".")

        return {
            rank:data.rank,
            jalinId:string[string.length-1],
            missionSolved:data.missionSolved,
            totalPoints:data.totalPoints
        }
    })

    return (
        <div className='base f-18 mt-4 table-ranking'>
                <Table borderless striped className="m-0">
                    <thead className='table-ranking-heading'>
                        <tr>
                            <th className='table-ranking-left-heading px-4'>Rank</th>
                            <th className='table-ranking-mid-heading px-4'>Username</th>
                            <th className='table-ranking-mid-heading px-4'>Points</th>
                            <th className='table-ranking-right-heading px-4'>Mission Solved</th>
                        </tr>
                    </thead>

                    <tbody className='table-ranking-body'>
                            {data.map((data,i) =>
                            i < 7 &&
                                <tr>
                                    <td className='table-ranking-items-rank py-4'>{data.rank}</td>
                                    <td className='table-ranking-items py-4 px-4'>
                                        <img src={rank4} alt="user rank 4" /> {data.jalinId.split(".")}
                                    </td>
                                    <td className='table-ranking-items py-4 px-4'>{data.totalPoints}</td>
                                    <td className='table-ranking-items py-4 px-4'>{data.missionSolved}</td>
                                </tr>
                                )}
                            
                    </tbody>
                </Table>
            </div>
    )
}

export default TableRanking
