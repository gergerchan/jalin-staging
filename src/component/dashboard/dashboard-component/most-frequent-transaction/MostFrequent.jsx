import { useState, useEffect } from 'react';
import axios from 'axios';
import Cookies from "js-cookie"
import ReactECharts from 'echarts-for-react';
import { BiCalendarCheck } from "react-icons/bi";
import "./mostFrequent.scss"

const MostFrequent = () => {
    const [ option, setOption] = useState(null);

    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}

    useEffect(() => {
        getDataMostTransaction()
    }, [])

    const getDataMostTransaction = async () => {
        const url = `https://jalin-app-backend.herokuapp.com/api/admin/v1/transactions/most`

        try {
            const response = await axios.get(url, headers)
            const option = {
                color: [
                    "#3EB7A1",
                    "#E15E54",
                    "#488CD3",
                    "#F4C14F"
                ],
                tooltip: {
                    trigger: 'item'
                },
                series: [
                    {
                        // name: 'Most Frequent Transaction',
                        type: 'pie',
                        radius: ['50%', '70%'],
                        avoidLabelOverlap: false,
                        label: {
                            show: false,
                            position: 'center'
                        },
                        labelLine: {
                            show: false
                        },
                        data: [
                            { value: response.data.transactionCountList[0].transactionCount+response.data.transactionCountList[1].transactionCount, name: 'Transfer' },
                            { value: response.data.transactionCountList[4].transactionCount, name: 'Payment Mobile Phone Credit' },
                            { value: response.data.transactionCountList[3].transactionCount, name: 'QR Payment' },
                            { value: response.data.transactionCountList[2].transactionCount, name: 'E-Wallet Top Up' }
                        ]
                    }
                ]
            };
            setOption(option)
        } catch (err) {
            console.log(err);
        }
    }

    return (
        <div className='base home-most-frequent-transaction'>
            <div className="row">
                <div className="most-frequent-transaction-chart col-5">
                    {option && 
                        <ReactECharts
                            option={option}
                        />
                    }
                </div>
                <div className="col-7">
                    <div className="card-transaction">
                        <p className='card-transaction-heading'>Transactions</p>
                        <ul className='card-transaction-check'>
                            <li><BiCalendarCheck
                                className='card-transaction-check-item'
                                color='#3EB7A1'
                                size='21px'
                            />
                                Transfer
                            </li>
                            <li><BiCalendarCheck
                                className='card-transaction-check-item'
                                color='#488CD3'
                                size='21px'
                            />
                                QR Payment
                            </li>
                            <li><BiCalendarCheck
                                className='card-transaction-check-item'
                                color='#E15E54'
                                size='21px'
                            />
                                Payment Mobile Phone Credit
                            </li>
                            <li><BiCalendarCheck
                                className='card-transaction-check-item'
                                color='#F4C14F'
                                size='21px'
                            />
                                E-Wallet Top Up
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MostFrequent
