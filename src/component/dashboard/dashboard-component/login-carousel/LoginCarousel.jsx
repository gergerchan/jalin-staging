import Slider from "react-slick";
import "./logincarousel.scss"
const LoginCarousel = (props) => {
  var settings = {
    dots: true,
    autoplay: true,
    speed: 1000,
    autoplaySpeed: 4000,
    infinite: true,
    cssEase: "linear",
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows:false,
    customPaging: (i) => (
      <div
        style={{
          marginTop: "10px",
          width: "10px",
          height: "10px",
          borderRadius: "20px",
          backgroundColor: "#c4c4c4"
        }}
      ></div>
    ),
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll:1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      }
    ]
  };

  return (
    <div className="login-carousel">
        <Slider  {...settings}>
          <div className="carousel-content ">
            <div className='d-flex justify-content-center'>
              <img src={props.img[0]} alt=""/>
            </div>
            <h3 className="b-700 mt-5">Savings Management</h3>
            <p>Auto debit and maximum interest rate will help you to conquer your dreams!</p> 
          </div>
          <div className="carousel-content ">
            <div className='d-flex justify-content-center'>
              <img src={props.img[1]} alt=""/>
            </div>
            <h3 className="b-700 mt-5">Easy Top-Up & Transaction</h3>
            <p>Enjoy the easiest way to handle all your every transaction with only one click away!</p> 
          </div>
          <div className="carousel-content ">
            <div className='d-flex justify-content-center'>
              <img src={props.img[2]} alt=""/>
            </div>
            <h3 className="b-700 mt-5">More Excitement & Satisfaction</h3>
            <p>Carry out missions and daily check-in every day and get you rank above your friends!</p> 
          </div>
        </Slider>
      </div>
  )
}

export default LoginCarousel