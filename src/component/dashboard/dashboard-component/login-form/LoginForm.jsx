import { useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Input, Form, FormGroup, Spinner} from 'reactstrap';
import { FaEyeSlash } from "react-icons/fa";
import axios from 'axios';
import qs from "qs"
import Cookies from "js-cookie"
import { useHistory } from "react-router-dom"
import { types } from '../../../../redux/action'
import "./loginform.scss"


const LoginForm = () => {
    let history = useHistory()
    const expireTime = new Date(new Date().getTime() + 120 * 60 * 1000);
    const [showpassword, setShowpassword] = useState("password")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [status, setStatus] = useState("")
    const dispatch = useDispatch()
    const loading = useSelector(state => state.loading)

    const showpass = () => {
        showpassword === "password" ?  setShowpassword("input") : setShowpassword("password")
    }

    const handleEmail = (e) => {
        setEmail(e.target.value)
    }

    const handlePassword = (e) => {
        setPassword(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch({type: types.LOADING})
        const url = `${process.env.REACT_APP_API}/login`
        axios.post(url,qs.stringify({
            email: email,
            password: password
        }))
        .then(res => {
            Cookies.set("TOKEN_USER", res.data.data.token, { expires: expireTime })
            Cookies.set("EMAIL_USER", res.data.data.email , { expires: expireTime })
            dispatch({type: types.NOT_LOADING})
            history.push("/dashboard");
        })
        .catch(err => {
            dispatch({type: types.NOT_LOADING})
            setStatus(err.response.data.message)
            
            console.log(err.response.data.message)
        })
    }


    return (
        <div className="login-form p-5">
            <h3  className='b-700 m-0'>Welcome</h3>
            <p className="mb-4">Sign in to your account</p>
            <Form onSubmit={handleSubmit}>
                <FormGroup className="my-4">
                    <Input data-testid="email" type="email" name="email" id="Email" className="px-3 py-2" placeholder="Email" onChange={handleEmail} required/>
                </FormGroup>
                <FormGroup className="mb-3 password">
                    <Input data-testid="password" type={showpassword} name="password" id="Password" className="px-3 py-2" placeholder="Password" onChange={handlePassword} required/>
                    <FaEyeSlash color="#999999d8" onClick={showpass}/>
                </FormGroup>
                {loading ?
                <FormGroup className="d-flex">
                      <Spinner color="primary" children=""/>
                </FormGroup> : status ? 
                <FormGroup className="d-flex">
                    <p style={{color: "red"}}>{(status === "Forbidden") ? "Please login as an Administrator" : "Invalid Email or Password"}</p>
                </FormGroup> :
                <>
                </>
                }
                
                <FormGroup className=" mt-5 mb-3">
                    <Button data-testid="login-button" className="py-3" >Sign In</Button>
                </FormGroup>
                
            </Form>
        </div>
    )
}

export default LoginForm
