import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import "./detailModal.scss";

const DetailMissionModals = ({isOpen, toggle, closeBtn, modalIcon, modalTitle, formLabel,data}) => {
    let cash
    if(data){
        cash = "RP. "+parseFloat(data.minAmount).toLocaleString('de-DE');
    }

    return (
        <div>
            {data && 
            <Modal className='base detail-modal' size='lg' isOpen={isOpen} toggle={toggle}>
                <ModalHeader className='detail-modal-heading border-bottom-0' toggle={toggle} close={closeBtn}>
                    <img className='detail-modal-icon' src={modalIcon} alt="modal icon" /> 
                    <span className='detail-modal-title'>{modalTitle}</span> 
                </ModalHeader>
                <ModalBody className='detail-modal-body'>
                    <div className="row detail-modal-container">
                        <div className="col-4">
                            <p className='detail-modal-label'>{formLabel}</p>
                            <p className='detail-modal-content'>{data.activity}</p>
                        </div>
                        <div className="col-5">
                            <p className='detail-modal-label'>Min. Amount Transaction</p>
                            <p className='detail-modal-content'>{cash}</p>
                        </div>
                        <div className="col-3">
                            <p className='detail-modal-label'>Status</p>
                            {data.status ? <p className='detail-modal-status detail-modal-active'>
                                <span>Active</span>
                            </p>
                                 :  <p className='detail-modal-status detail-modal-inactive' >
                                 <span>Inactive</span>
                             </p>}
                        </div>
                    </div>

                    <div className="row detail-modal-container">
                        <div className="col-4">
                            <p className='detail-modal-label'>Frequency</p>
                            <p className='detail-modal-content'>{data.frequency}</p>
                        </div>
                        <div className="col-5">
                            <p className='detail-modal-label'>Period</p>
                            <p className='detail-modal-content'>{data.expiration}</p>
                        </div>
                        <div className="col-3">
                            <p className='detail-modal-label'>Points</p>
                            <p className='detail-modal-content'>{data.point}</p>
                        </div>
                        {/* <div className="col-3">
                            <p className='detail-modal-label'>Payment Gateway</p>
                            <p className='detail-modal-content'>PayPal</p>
                        </div> */}
                    </div>

                    {/* <div className="row detail-modal-container">
                        <div className="col-4">
                            <p className='detail-modal-label'>Points</p>
                            <p className='detail-modal-content'>{data.point}</p>
                        </div>
                        <div className="col-4">
                            <p className='detail-modal-label'>Start Date</p>
                            <p className='detail-modal-content'>31-06-2021</p>
                        </div>
                    </div> */}

                    <hr />

                    <div className="row detail-modal-container">
                        <div>
                            <p className='detail-modal-label'>Description</p>
                            <p className='detail-modal-content mission-p-enter'>{data.missionDescription}</p>
                        </div>
                    </div>
                    <div className="row detail-modal-container">
                        <div>
                            <p className='detail-modal-label'>Terms and Condition</p>
                            <p className='detail-modal-content mission-p-enter'>{data.tncDescription}</p>
                        </div>
                    </div>

                    {/* <div className="row detail-modal-container">
                        <div>
                            <p className='detail-modal-label'>Terms and Condition</p>
                            {data.tncDescription}
                            <ol className='detail-modal-list detail-modal-content'>
                                <li></li>
                                <li>The voucher can be redeemed until the date stated on this voucher, 
                                    or as long as the voucher quota is still available (whichever comes first).
                                </li>
                                <li>Valid only for one transaction.</li>
                                <li>The voucher cannot be used with other promotions.</li>
                                <li>The voucher that has been received cannot be swapped/exchanged with other 
                                    vouchers (non-exchangeable), cannot be returned, and cannot be refunded, 
                                    no matter the amount.
                                </li>
                                <li>JALIN has the right to cancel the transaction/cancel the use of vouchers of any time, 
                                    without prior notice to the user.
                                </li>
                                <li>
                                    JALIN has the right to change the terms and conditions of the promo at any time without prior notice.
                                </li>
                            </ol>
                        </div>
                    </div> */}
                </ModalBody>
            </Modal>
            }
        </div>
    )
}

export default DetailMissionModals