import { useEffect, useState } from 'react';
import { Modal, ModalHeader, ModalBody, Input, Label, FormGroup, Form } from 'reactstrap';
import "./pointsRewardsModals.scss";
import { useDispatch } from 'react-redux'
import { editMission } from  "../../../../redux/voucher-action"

const EditModals = ({isOpen, toggle, closeBtn, modalIcon, modalTitle, formLabel,data, toggleDesc}) => {
    
    const [mission, setMission] = useState(null)
    const [frequency, setFrequency] = useState(null)
    const [point, setPoint] = useState(null)
    const [period, setPeriod] = useState(null)
    const [tncdesc, setTncdesc] = useState(null)
    const [missiondesc, setMissiondesc] = useState(null)
    const [amount, setAmount] = useState(null)
    const dispatch = useDispatch()
    const [done, setDone] = useState(false)

    useEffect(() => {
        dataExist()
    }, [data])

    const dataExist = () => {
        if (data) {
            if(data.activity==="Top Up Mobile Credit")
        {
            setMission("PAYMENT_MOBILE_PHONE_CREDIT")
        } else if(data.activity==="Pay (QR Code) with Jalin when shopping in merchant partner")
        {
            setMission("PAYMENT_QR")
        } else if(data.activity==="Top Up E-Wallet")
        {
            setMission("TOP_UP")
        } else if(data.activity==="Transfer to other Jalin Account")
        {
            setMission("TRANSFER")
        } else if(data.activity==="Pay bill (electricity, water, telephone, internet)")
        {
            setMission("PAYMENT_BILL")
        } else if(data.activity==="Transfer to other Bank")
        {
            setMission("TRANSFER_DOMESTIC")
        } else if(data.activity==="Top Up Mobile Data")
        {
            setMission("PAYMENT_MOBILE_PHONE_DATA")
        }
            setFrequency(data.frequency.split(" ")[0])
            setPoint(data.point)
            setPeriod(data.expiration)
            setTncdesc(data.tncDescription)
            setMissiondesc(data.missionDescription)
            setAmount(data.minAmount)
            if(data.status === true){
                setDone("TRUE")
            } else{
                setDone("FALSE")
            }
            
        }
    }

    const inputMission = (e) =>{
        e.preventDefault()
        setMission(e.target.value)
    }
    const inputFrequency = (e) =>{
        e.preventDefault()
        let str = e.target.value
        str = str.replace(/\D/g, "");
        setFrequency(str)
    }
    const inputPoint = (e) =>{
        e.preventDefault()
        let str = e.target.value
        str = str.replace(/\D/g, "");
        setPoint(str)
    }
    const inputPeriod = (e) =>{
        e.preventDefault()
        setPeriod(e.target.value)
    }
    const inputDescription = (e) =>{
        e.preventDefault()
        setMissiondesc(e.target.value)
    }
    const inputTnc = (e) =>{
        e.preventDefault()
        setTncdesc(e.target.value)
    }
    const inputAmount = (e) =>{
        e.preventDefault()
        let str = e.target.value
        str = str.replace(/\D/g, "");
        setAmount(str)
    }
    const inputStatus = (e) =>{
        e.preventDefault()
        setDone(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        let statusBool = (done === "TRUE") 
        if(modalTitle === "Edit Mission List") 
        {
        if(done){
            let dataedit={
                id:data.id,
                activity: mission,
                missionDescription: missiondesc,
                tncDescription: tncdesc,
                frequency: parseInt(frequency),
                minAmount: amount,
                expiration: period,
                point: parseInt(point),
                status:statusBool
            }
            console.log(dataedit);
            dispatch(editMission(dataedit))
        }
        }
    }
    return (
        <div>
            {data &&
            <Modal className='base points-rewards-modals' size='xl' isOpen={isOpen} toggle={toggle}>
                <ModalHeader className='points-rewards-heading border-bottom-0' toggle={toggle} close={closeBtn}>
                    <img className='points-rewards-icon' src={modalIcon} alt="modal icon" /> 
                    <span className='points-rewards-title'>{modalTitle}</span> 
                </ModalHeader>
                <ModalBody className='points-rewards-body'>
                    <Form onSubmit={handleSubmit}>
                        <div className="row pb-4">
                            <div className="col-5">
                                <label className='points-rewards-label' htmlFor="examplePeriod">{formLabel}</label>
                                <select id="mission-edit-list" class="form-select" aria-label="Default select example" value={mission} onChange={inputMission}>
                                    <option value="" disabled selected>{`Select ${toggleDesc} list`}</option>
                                    <option value="TOP_UP">Top Up E-Wallet</option>
                                    <option value="PAYMENT_QR" >Pay (QR Code) with Jalin when shopping in merchant partner</option>
                                    <option value="TRANSFER">Transfer to other Jalin Account</option>
                                    <option value="TRANSFER_DOMESTIC">Transfer to other Bank</option>
                                    <option value="PAYMENT_BILL">Pay bill (electricity, water, telephone, internet)</option>
                                    <option value="PAYMENT_MOBILE_PHONE_CREDIT">Top Up Mobile Credit</option>
                                    <option value="PAYMENT_MOBILE_PHONE_DATA">Top Up Mobile Data</option>
                                </select>
                            </div>
                            <FormGroup className='col-2'>
                                <Label className='points-rewards-label' for="exampleFrequency">Frequency</Label>
                                <Input type="number"  onChange={inputFrequency} value={frequency} placeholder="Input frequency" required/>
                            </FormGroup>
                            <FormGroup className='col-2'>
                                <Label className='points-rewards-label' for="examplePoints">Points</Label>
                                <Input type="number" onChange={inputPoint} value={point} placeholder="Input total points" required/>
                            </FormGroup>
                            <FormGroup className='col-3'>
                                <Label className='points-rewards-label' for="exampleAmount">Min. Amount Transaction</Label>
                                <Input type="number" onChange={inputAmount} value={amount} placeholder="Input Min. Amount" required/>
                            </FormGroup>
                        </div>

                        <div className="row pb-4">
                            <div className="col-4">
                                <label className='points-rewards-label' htmlFor="examplePeriod">Period</label>
                                <select id="mission-edit-period" class="form-select" aria-label="Default select example" value={period} onChange={inputPeriod}>
                                    <option value="WEEKLY">Weekly</option>
                                    <option value="BIWEEKLY">Biweekly</option>
                                    <option value="MONTHLY">Monthly</option>
                                </select>
                            </div>
                            <div className="col-4">
                                <label className='points-rewards-label' htmlFor="exampleStatus">Status</label>
                                <select id="mission-edit-status" class="form-select" aria-label="Default select example" value={done} onChange={inputStatus}>
                                    <option value="TRUE" selected>Active</option>
                                    <option value="FALSE">Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div className="row pb-4 point-rewards-textarea-box">
                            <label class="points-rewards-label point-rewards-textarea-label">Description</label>
                            <textarea class="point-rewards-textarea" placeholder='Enter the description here' onChange={inputDescription} value={missiondesc} rows="5" required></textarea>
                        </div>

                        <div className="row pb-4 point-rewards-textarea-box">
                            <label class="points-rewards-label point-rewards-textarea-label">Terms and Condition</label>
                            <textarea class="point-rewards-textarea" placeholder='Type mission’s terms and condition here' onChange={inputTnc} value={tncdesc} rows="5" required></textarea>
                        </div>

                        <div className="row pt-4">
                            <div className="col-1"><button className='btn-accept'>Update</button></div>
                            <div className="col-2 btn-back-lg d-flex justify-content-center"><button onClick={toggle} className='btn-back'>Back</button></div> 
                        </div>
                    </Form>
                </ModalBody>
            </Modal>
            }
        </div>
    )
}

export default EditModals
