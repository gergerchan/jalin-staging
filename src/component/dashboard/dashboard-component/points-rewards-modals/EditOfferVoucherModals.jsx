import { useEffect, useState } from 'react';
import { Modal, ModalHeader, ModalBody, Input, Label, FormGroup, Form } from 'reactstrap';
import "./pointsRewardsModals.scss";
import { useDispatch } from 'react-redux'
import { addOffer, editOffer } from  "../../../../redux/voucher-action"

import { dateNow } from '../../../../services/helpers';

const EditOfferVoucherModals = ({isOpen, toggle, closeBtn, modalIcon, modalTitle, formLabel,data, toggleDesc,placeholder ,voucher}) => {
    const [usage, setUsage] = useState("TRANSFER_DOMESTIC")
    const [quota, setQuota] = useState(null)
    const [point, setPoint] = useState(null)
    const [tncdesc, setTncdesc] = useState(null)
    const [validity, setValidity] = useState(dateNow())
    const [status, setStatus] = useState("TRUE")
    const [id, setId] = useState(null)
    const [amount, setAmount] = useState(null)

    const dispatch = useDispatch()

    useEffect(() => {
        dataExist()
    }, [data])

    const dataExist = () => {
        if (data) {
            
            setUsage(data.usage)
            setQuota(data.quota)
            setPoint(data.points)
            setTncdesc(data.tncDescription)
            setValidity(data.validity)
            data.status ? setStatus("TRUE") : setStatus("FALSE")
            setId(data.id)
            setAmount(data.amount)
        }
    }   

    const inputUsage = (e) =>{
        e.preventDefault()
        setUsage(e.target.value)
    }
    const inputQuota = (e) =>{
        e.preventDefault()
        let str = e.target.value
        str = str.replace(/\D/g, "");
        voucher ? 
        setAmount(str) : setQuota(str)
    }
    const inputPoint = (e) =>{
        e.preventDefault()
        let str = e.target.value
        str = str.replace(/\D/g, "");
        setPoint(str)
    }
    const inputTnc = (e) =>{
        e.preventDefault()
        setTncdesc(e.target.value)
    }
    const inputValidity= (e) =>{
        e.preventDefault()
        console.log(e.target.value,dateNow());
        (e.target.value < dateNow()) ? 
            setValidity(dateNow())
         : setValidity(e.target.value)
        
    }
    const inputStatus= (e) =>{
        e.preventDefault()
        setStatus(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if(modalTitle === "Edit Offer List") 
        {
        let statusBool = (status === "TRUE") 
        let data={
            id:id,
            category: "JALIN_OFFER",
            usage: usage,
            tncDescription: tncdesc,
            quota: parseInt(quota),
            validity: validity,
            points: parseInt(point),
            status:statusBool,
            amount:amount
        }
        dispatch(editOffer(data))
        toggle()
        }
        if(voucher) 
        {
        let statusBool = (status === "TRUE") 
        let data={
            id:id,
            category: "VOUCHER",
            usage: usage,
            tncDescription: tncdesc,
            quota: parseInt(quota),
            validity: validity,
            points: parseInt(point),
            status:statusBool,
            amount:amount
        }
        dispatch(editOffer(data))
        toggle()
        }
    }
    return (
        <div>
            <Modal className='base points-rewards-modals' size='xl' isOpen={isOpen} toggle={toggle}>
                <ModalHeader className='points-rewards-heading border-bottom-0' toggle={toggle} close={closeBtn}>
                    <img className='points-rewards-icon' src={modalIcon} alt="modal icon" /> 
                    <span className='points-rewards-title'>{modalTitle}</span> 
                </ModalHeader>
                <ModalBody className='points-rewards-body'>
                    <Form onSubmit={handleSubmit}>
                        <div className="row pb-4">
                            <div className="col-6">
                                <label className='points-rewards-label' htmlFor="examplePeriod">{formLabel.label1}</label>
                                <select id="offer-voucher-edit-list" className="form-select" aria-label="Default select example" value={usage} onChange={inputUsage}>
                                {voucher ? <>
                                        <option value="PAYMENT_MOBILE_PHONE_DATA">Discount payment mobile data Tsel or Indosat or SmartFren, or Three quota </option>
                                        <option value="PAYMENT_MOBILE_PHONE_CREDIT">Discount mobile credit any operator</option>
                                        </>
                                    : <>
                                        <option value="TRANSFER_DOMESTIC">Free transfer to any bank</option>
                                            <option value="TOP_UP" >Free top up e-wallet e-wallet admin fee</option>
                                            <option value="PAYMENT_BILL_PLN">Free electricity payment admin fee</option>
                                            <option value="PAYMENT_BILL_PDAM">Free PDAM payment admin fee</option>
                                            <option value="PAYMENT_BILL_INTERNET">Free internet provider payment admin fee</option>
                                            <option value="PAYMENT_MOBILE_PHONE_CREDIT_25000">Free IDR 25000 additional credit to any operator</option>
                                            <option value="PAYMENT_MOBILE_PHONE_CREDIT_15000">Free IDR 15000 additional credit to any operator</option>
                                            <option value="PAYMENT_MOBILE_PHONE_DATA_5GB">Free 5GB additional internet data for telkomsel, indosat, smartfren, and three operators</option>
                                        </>
                                    }
                                </select>
                            </div>
                            <FormGroup className='col-3'>
                                <Label className='points-rewards-label' for="exampleFrequency">{formLabel.label2}</Label>
                                <Input type="number" value={voucher ?amount : quota }  onChange={inputQuota} placeholder={`Input ${placeholder}`} required/>
                            </FormGroup>
                            <FormGroup className='col-3'>
                                <Label className='points-rewards-label' for="examplePoints">Points</Label>
                                <Input type="number" value={point} onChange={inputPoint}  placeholder="Input total points" required/>
                            </FormGroup>
                        </div>

                        <div className="row pb-4">
                            <div className="col-3">
                                <label className='points-rewards-label' htmlFor="exampleValid">Valid Until</label>
                                <p><input type="date"className="form-date p-2" value={validity} onChange={inputValidity} /></p>
                            </div>
                            <div className="col-3">
                                <label className='points-rewards-label' htmlFor="exampleStatus">Status</label>
                                <select id="offer-voucher-edit-list" className="form-select" value={status} aria-label="Default select example" onChange={inputStatus}>
                                    <option value="TRUE" selected>Active</option>
                                    <option value="FALSE">Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div className="row pb-4 point-rewards-textarea-box">
                            <label className="points-rewards-label point-rewards-textarea-label">Terms and Condition</label>
                            <textarea className="point-rewards-textarea" value={tncdesc} placeholder={`Type ${toggleDesc}’s terms and condition here`} onChange={inputTnc} rows="5" required></textarea>
                        </div>

                        <div className="row pt-4">
                            <div className="col-1"><button type="submit" className='btn-accept'>Update</button></div>
                            <div className="col-2 btn-back-lg d-flex justify-content-center"><button type="reset" onClick={toggle} className='btn-back'>Back</button></div> 
                        </div>
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    )
}

export default EditOfferVoucherModals
