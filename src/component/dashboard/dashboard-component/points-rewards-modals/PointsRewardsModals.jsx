import { useState } from 'react';
import { Modal, ModalHeader, ModalBody, Input, Label, FormGroup, Form } from 'reactstrap';
import "./pointsRewardsModals.scss";
import { useDispatch } from 'react-redux'
import { addMission } from  "../../../../redux/voucher-action"

const PointsRewardsModals = ({isOpen, toggle, closeBtn, modalIcon, modalTitle, toggleDesc, formLabel}) => {
    const [mission, setMission] = useState("PAYMENT_QR")
    const [frequency, setFrequency] = useState(null)
    const [point, setPoint] = useState(null)
    const [period, setPeriod] = useState("WEEKLY")
    const [tncdesc, setTncdesc] = useState("No Terms and Condition")
    const [missiondesc, setMissiondesc] = useState("No Desc")
    const [status, setStatus] = useState(true)
    const [amount, setAmount] = useState(null)
    const dispatch = useDispatch()

    const inputMission = (e) =>{
        e.preventDefault()
        setMission(e.target.value)
        
        if(mission==="PAYMENT_MOBILE_PHONE_CREDIT")
        {
            setTncdesc("Minimal transaksi Rp 20.000")
        } else if(mission==="PAYMENT_QR")
        {
            setTncdesc("Minimal transaksi Rp 20.000")
        } else if(mission==="TOP_UP")
        {
            setTncdesc("Minimal transaksi Rp 50.000")
        } else if(mission==="TRANSFER")
        {
            setTncdesc("Minimal transaksi Rp 50.000")
        } else if(mission==="PAYMENT_BILL")
        {
            setTncdesc("Tanpa minimal transaksi")
        } else if(mission==="TRANSFER_DOMESTIC")
        {
            setTncdesc("Minimal transaksi Rp 50.000")
        } else if(mission==="PAYMENT_MOBILE_PHONE_DATA")
        {
            setTncdesc("Minimal transaksi Rp 20.000")
        }
    }
    const inputFrequency = (e) =>{
        e.preventDefault()
        let str = e.target.value
        str = str.replace(/\D/g, "");
        setFrequency(str)
    }
    const inputPoint = (e) =>{
        e.preventDefault()
        let str = e.target.value
        str = str.replace(/\D/g, "");
        setPoint(str)
    }
    const inputPeriod = (e) =>{
        e.preventDefault()
        setPeriod(e.target.value)
    }
    const inputDescription = (e) =>{
        e.preventDefault()
        setMissiondesc(e.target.value)
    }
    const inputTnc = (e) =>{
        e.preventDefault()
        setTncdesc(e.target.value)
    }
    const inputAmount = (e) =>{
        e.preventDefault()
        let str = e.target.value
        str = str.replace(/\D/g, "");
        setAmount(str)
    }
    const inputStatus = (e) =>{
        e.preventDefault()
        setStatus(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        let statusBool = (status === "TRUE") 
        if(modalTitle === "Add Mission List") 
        {
        let data={
            activity: mission,
            missionDescription: missiondesc,
            tncDescription: tncdesc,
            frequency: parseInt(frequency),
            minAmount: amount,
            expiration: period,
            point: parseInt(point),
            status:statusBool
        }
        dispatch(addMission(data))
        }
    }
    
    return (
        <div>
            <Modal className='base points-rewards-modals' size='xl' isOpen={isOpen} toggle={toggle}>
                <ModalHeader className='points-rewards-heading border-bottom-0' toggle={toggle} close={closeBtn}>
                    <img className='points-rewards-icon' src={modalIcon} alt="modal icon" /> 
                    <span className='points-rewards-title'>{modalTitle}</span> 
                </ModalHeader>
                <ModalBody className='points-rewards-body'>
                    <Form onSubmit={handleSubmit}>
                        <div className="row pb-4">
                            <div className="col-5">
                                <label className='points-rewards-label' htmlFor="examplePeriod">{formLabel}</label>
                                <select id="mission-list" class="form-select" aria-label="Default select example" onChange={inputMission}>
                                    <option value="" disabled selected>{`Select ${toggleDesc} list`}</option>
                                    <option value="TOP_UP">Top Up E-Wallet</option>
                                    <option value="PAYMENT_QR" >Pay (QR Code) with Jalin when shopping in merchant partner</option>
                                    <option value="TRANSFER">Transfer to other Jalin Account</option>
                                    <option value="TRANSFER_DOMESTIC">Transfer to other Bank</option>
                                    <option value="PAYMENT_BILL">Pay bill (electricity, water, telephone, internet)</option>
                                    <option value="PAYMENT_MOBILE_PHONE_CREDIT">Top Up Mobile Credit</option>
                                    <option value="PAYMENT_MOBILE_PHONE_DATA">Top Up Mobile Data</option>
                                </select>
                            </div>
                            <FormGroup className='col-2'>
                                <Label className='points-rewards-label' for="exampleFrequency">Frequency</Label>
                                <Input type="number" onChange={inputFrequency} value={frequency} placeholder="Input frequency" required/>
                            </FormGroup>
                            <FormGroup className='col-2'>
                                <Label className='points-rewards-label' for="examplePoints">Points</Label>
                                <Input type="number" onChange={inputPoint} value={point} placeholder="Input total points" required/>
                            </FormGroup>
                            <FormGroup className='col-3'>
                                <Label className='points-rewards-label' for="exampleAmount">Min. Amount Transaction</Label>
                                <Input type="number" onChange={inputAmount} value={amount} placeholder="Input Min. Amount" required/>
                            </FormGroup>
                        </div>

                        <div className="row pb-4">
                            <div className="col-4">
                                <label className='points-rewards-label' htmlFor="examplePeriod">Period</label>
                                <select id="mission-period" class="form-select" aria-label="Default select example" onChange={inputPeriod}>
                                    <option value="WEEKLY" disabled selected>Week(s)</option>
                                    <option value="WEEKLY">Weekly</option>
                                    <option value="BIWEEKLY">Biweekly</option>
                                    <option value="MONTHLY">Monthly</option>
                                </select>
                            </div>
                            <div className="col-4">
                                <label className='points-rewards-label' htmlFor="exampleStatus">Status</label>
                                <select id="mission-status" class="form-select" aria-label="Default select example" onChange={inputStatus}>
                                    <option value="TRUE" selected>Active</option>
                                    <option value="FALSE">Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div className="row pb-4 point-rewards-textarea-box">
                            <label class="points-rewards-label point-rewards-textarea-label">Description</label>
                            <textarea class="point-rewards-textarea" placeholder='Enter the description here' onChange={inputDescription} rows="5" required></textarea>
                        </div>

                        <div className="row pb-4 point-rewards-textarea-box">
                            <label class="points-rewards-label point-rewards-textarea-label">Terms and Condition</label>
                            <textarea class="point-rewards-textarea" placeholder='Type mission’s terms and condition here' onChange={inputTnc} rows="5" required></textarea>
                        </div>

                        <div className="row pt-4">
                            <div className="col-1"><button type="submit" className='btn-accept'>Save</button></div>
                            <div className="col-2 btn-back-lg d-flex justify-content-center"><button type="reset" onClick={toggle} className='btn-back'>Back</button></div> 
                        </div>
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    )
}

export default PointsRewardsModals
