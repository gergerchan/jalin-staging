import { useDispatch } from 'react-redux';
import { Modal, ModalBody } from 'reactstrap';
import deleteIcon from "../../../../asset/img/delete-modals-icon.svg";
import "./deleteModals.scss";
import { deleteMission,deleteOffer } from  "../../../../redux/voucher-action"

const DeleteModals = ({isOpen, toggle, toggleDesc, data,mission,Offer }) => {
    const dispatch = useDispatch()
    const handleDelete = (e) =>{
        e.preventDefault()
        mission && dispatch(deleteMission(data.id))
        Offer && dispatch(deleteOffer(data.id))
    }
    return (
        <div>
           <Modal className='base delete-modals' isOpen={isOpen} toggle={toggle}>
                <ModalBody className='delete-modals-body py-5 text-center'>
                    
                
                    <img src={deleteIcon} alt="delete icon modals" />
                    <h4 className='my-3' style={{fontWeight: '700'}}>Are you sure?</h4>
                    <p className='mb-4'>You will not be able to recover the deleted {toggleDesc}!</p>
                    <div className="row delete-modals-body-btn">
                        <div className="delete-modals-body-btn-yes col-6 px-0">
                        <form  onSubmit={handleDelete}>
                            <button onClick={toggle} className='btn-yesdelete'>Yes, delete it!</button>
                        </form>
                        </div>
                        <div className="delete-modals-body-btn-no col-6 px-0">
                            <button onClick={toggle} className='btn-delete'>No, cancel please</button>
                        </div>
                    </div>
                
                </ModalBody>
            </Modal> 
        </div>
    )
}

export default DeleteModals
