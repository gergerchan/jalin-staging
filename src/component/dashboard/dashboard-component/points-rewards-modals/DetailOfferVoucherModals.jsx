import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import "./detailModal.scss";

const DetailOfferVoucherModals = ({isOpen, toggle, closeBtn, modalIcon, modalTitle, formLabel, description, dataOffer, dataVoucher }) => {

    return (
        <div>
            <Modal className='base detail-modal' size='lg' isOpen={isOpen} toggle={toggle}>
                <ModalHeader className='detail-modal-heading border-bottom-0' toggle={toggle} close={closeBtn}>
                    <img className='detail-modal-icon' src={modalIcon} alt="modal icon" /> 
                    <span className='detail-modal-title'>{modalTitle}</span> 
                </ModalHeader>
                <ModalBody className='detail-modal-body'>
                    <div className="row detail-modal-container">
                        <div className="col-4">
                            <p className='detail-modal-label'>{formLabel.label1}</p>
                            <p className='detail-modal-content'>{dataOffer && dataOffer.title}{dataVoucher && dataVoucher.title}</p>
                        </div>
                        <div className="col-5">
                            <p className='detail-modal-label'>{formLabel.label3}</p>
                            <p className='detail-modal-content'>{dataOffer && dataOffer.points}{dataVoucher && `Rp. ${parseFloat(dataVoucher.amount).toLocaleString('de-DE')}`}</p>
                        </div>
                        <div className="col-3">
                            <p className='detail-modal-label'>{formLabel.label4}</p>
                            <p className='detail-modal-content'>{dataOffer && `${dataOffer.quota} times`} {dataVoucher && dataVoucher.points}</p>
                        </div>
                    </div>

                    <div className="row detail-modal-container">
                        {/* <div className="col-4">
                            <p className='detail-modal-label'>Used For</p>
                            <p className='detail-modal-content'>{dataOffer && dataOffer.tncDescription}</p>
                        </div> */}
                        <div className="col-4">
                            <p className='detail-modal-label'>{formLabel.label2}</p>
                            <p className='detail-modal-content'>{dataOffer && dataOffer.validity}{dataVoucher && dataVoucher.validity}</p>
                        </div>
                        <div className="col-3">
                            <p className='detail-modal-label'>Status</p>
                            { dataOffer ?  dataOffer.status ?
                                <p className='detail-modal-status detail-modal-active'>
                                    <span>Active</span>
                                </p>
                                    :  
                                <p className='detail-modal-status detail-modal-inactive' >
                                    <span>Inactive</span>
                                </p> 
                                : <></>
                             }
                            { dataVoucher ? dataVoucher.status ?
                                <p className='detail-modal-status detail-modal-active'>
                                    <span>Active</span>
                                </p>
                                    :  
                                <p className='detail-modal-status detail-modal-inactive' >
                                    <span>Inactive</span>
                                </p> 
                                : <></>
                             }
                        </div>
                    </div>

                    <hr />

                    <div className="row detail-modal-container">
                        <div>
                            <p className='detail-modal-label'>Terms and Condition</p>
                            <p className='detail-modal-content detail-enter'>{dataOffer && dataOffer.tncDescription}{ dataVoucher && dataVoucher.tncDescription }</p>
                        </div>
                    </div>
                    {/* <div className="row detail-modal-container">
                        <div>
                            <p className='detail-modal-label'>Description</p>
                            <p className='detail-modal-content'>{description}</p>
                        </div>
                    </div> */}

                    {/* <div className="row detail-modal-container">
                        <div>
                            <p className='detail-modal-label'>Terms and Condition</p>
                            <ol className='detail-modal-list detail-modal-content'>
                                <li>The voucher can be redeemed until the date stated on this voucher, 
                                    or as long as the voucher quota is still available (whichever comes first).
                                </li>
                                <li>Valid only for one transaction.</li>
                                <li>The voucher cannot be used with other promotions.</li>
                                <li>The voucher that has been received cannot be swapped/exchanged with other 
                                    vouchers (non-exchangeable), cannot be returned, and cannot be refunded, 
                                    no matter the amount.
                                </li>
                                <li>JALIN has the right to cancel the transaction/cancel the use of vouchers of any time, 
                                    without prior notice to the user.
                                </li>
                                <li>
                                    JALIN has the right to change the terms and conditions of the promo at any time without prior notice.
                                </li>
                            </ol>
                        </div>
                    </div> */}
                </ModalBody>
            </Modal>
        </div>
    )
}

export default DetailOfferVoucherModals