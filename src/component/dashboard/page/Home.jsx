import Calendardashboard from "../dashboard-component/calendar-home/Calendardashboard"
import CashChart from "../dashboard-component/cash-chart/CashChart"
import MostFrequent from "../dashboard-component/most-frequent-transaction/MostFrequent"
import Profile from '../dashboard-component/profile/Profile'
import Sidebar from '../dashboard-component/sidebar/Sidebar'
import CardRequest from '../dashboard-component/card-request/CardRequest'
import "./position.scss"
import TitleSearcbar from "../dashboard-component/title-searchbar/TitleAndSearchbar"
import { Container } from "reactstrap"
import RecentTransaction from "../dashboard-component/recent-transaction/RecentTransction"
import UserCheckin from "../dashboard-component/user-app-checkin/UserCheckin"
import CardInfo from "../dashboard-component/key-cardinfo/Card-info"
import Icon1 from "../dashboard-component/key-cardinfo/icon-card/icon1.svg"
import Icon2 from "../dashboard-component/key-cardinfo/icon-card/icon2.svg"
import Icon3 from "../dashboard-component/key-cardinfo/icon-card/icon3.svg"
import Icon4 from "../dashboard-component/key-cardinfo/icon-card/icon4.svg"
import { useEffect } from "react"
import Cookies from "js-cookie"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from 'react-redux'
import { CalendarData, DashboardCard, dashboardRecentTrans, dashboardCheckin, DashboardCash,types } from "../../../redux/action"
import { Spinner } from 'reactstrap';

const Home = () => {
    let history = useHistory()
    const dispatch = useDispatch()
    const cardloading = useSelector(state => state.isLoading1)
    const calendarloading = useSelector(state => state.isLoading2)
    const recenttransloading = useSelector(state => state.isLoading3)
    const checkinloading = useSelector(state => state.isLoading4)
    const cashloading = useSelector(state => state.isLoading5)
    const cardData = useSelector(state => state.cardData)
    const cashData = useSelector(state => state.cash)
    const calendardata = useSelector(state => state.calendar)
    const recentTrans = useSelector(state => state.dashboardTransData)
    const checkIn = useSelector(state => state.dashboardCheckIn)
    // NEED FIXING MOST FREQUENT TRANSACTION

    useEffect(() => {
        isLogin()
        dispatch({type:types.ALL_NOT_LOADING})
        dispatch(DashboardCard())
        dispatch(DashboardCash())
        dispatch(CalendarData())
        dispatch(dashboardRecentTrans())
        dispatch(dashboardCheckin())
    }, [])

    const isLogin = () => {
        if (typeof Cookies.get("TOKEN_USER") === "undefined") {
            history.push("/");
            return true;
        }
    }

    return (
        <div>
            <div className="row m-0">
                <div className="col-lg-2 p-0">
                    <Sidebar data-testid="sidebar"/>
                </div>
                <div className="col-lg-7">
                    <Container>
                        <TitleSearcbar title="Dashboard" />
                        <div className="row mt-3">
                            {cardloading &&
                                <div className="col-6 mt-3">
                                    <Spinner color="primary" children="" />
                                </div>
                            }
                            {!cardloading && cardData &&
                                <>
                                    <div className="col-6 mt-3">
                                        <CardInfo
                                            title="Active Customer"
                                            number={cardData.activecust}
                                            icon={Icon1}
                                        />
                                    </div>
                                    <div className="col-6 mt-3">
                                        <CardInfo
                                            title="Today’s Check In"
                                            number={`${cardData.mission.checkInPercentage}%`}
                                            number2={`(${cardData.mission.totalCheckIn})`}
                                            icon={Icon2}
                                        />
                                    </div>
                                    <div className="col-6 mt-3">
                                        <CardInfo
                                            title="Mission Taken"
                                            number={cardData.MissionTaken}
                                            icon={Icon3}
                                        />
                                    </div>
                                    <div className="col-6 mt-3">
                                        <CardInfo
                                            title="Mission Completed"
                                            number={`${cardData.percent}%`}
                                            number2={`(${cardData.missionComplete})`}
                                            icon={Icon4}
                                        />
                                    </div>
                                </>
                            }
                        </div>
                        {cashloading &&
                            <Spinner color="primary" children="" className="mt-3" />
                        }
                        {!cashloading && cashData && <>
                            <h4 className="b-700 mt-4">Cash In & Cash Out</h4>
                            <CashChart data={cashData}/>
                        </>
                        }

                        <h4 className="b-700 mt-4">Most Frequent Transaction</h4>
                        <MostFrequent />

                        <h4 className="b-700 mt-4">Check In & Mission Completion</h4>
                        {calendarloading &&
                            <Spinner color="primary" children="" className="mt-3" />
                        }
                        {!calendarloading && calendardata && <>
                            <Calendardashboard data={calendardata} />
                        </>
                        }
                        {recenttransloading &&
                            <p>
                                <Spinner color="primary" children="" className="mt-3" />
                            </p>
                        }
                        {!recenttransloading && recentTrans &&
                            <RecentTransaction data={recentTrans} />
                        }
                        {checkinloading &&
                            <p>
                                <Spinner color="primary" children="" className="mt-3" />
                            </p>
                        }
                        {!checkinloading && checkIn &&
                            <UserCheckin data={checkIn} />
                        }
                    </Container>
                </div>
                <div className="col-lg-3 mt-3">
                    <div className="pos-fixed b-left">
                        <div className="row p-3 mb-4">
                            <div className="col-lg-12">
                                <div className="">
                                    <Profile />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <CardRequest />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home
