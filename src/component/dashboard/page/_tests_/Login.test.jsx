import Login from "../Login"
import { render, fireEvent } from "@testing-library/react"
import { Provider } from 'react-redux'
import globalStore from '../../../../redux/store'
import { BrowserRouter } from "react-router-dom"

let getByTestId
beforeEach(() => {
  const component = render(
  <Provider store={globalStore}>
    <BrowserRouter>
      <Login/>
    </BrowserRouter>
  </Provider>)
  getByTestId = component.getByTestId
})

test("Input Email should be email type",() => {
  const emailEl = getByTestId("email")
  expect(emailEl).toBeInTheDocument();
  expect(emailEl).toHaveAttribute("type", "email");
})

test("Input password should be password",() => {
  const passEl = getByTestId("password")
  expect(passEl).toBeInTheDocument();
  expect(passEl).toHaveAttribute("type", "password");
})

test("Login Button",() => {
  const buttonEl = getByTestId("login-button")
  expect(buttonEl).toBeInTheDocument();
  expect(buttonEl).toHaveTextContent("Sign In");
})

test("Email Test",() => {
    const emailEl = getByTestId("email")
    fireEvent.change(emailEl, {
        target: { value: "tonystark" }
      })
    expect(emailEl.value).toBe("tonystark")
})

test("Password Test",() => {
  const passEl = getByTestId("password")
  fireEvent.change(passEl, {
      target: { value: "password" }
    })
  expect(passEl.value).toBe("password")
})
