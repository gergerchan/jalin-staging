import Cookies from "js-cookie";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Container } from "reactstrap"
import Sidebar from '../dashboard-component/sidebar/Sidebar'
import TitleSearcbar from "../dashboard-component/title-searchbar/TitleAndSearchbar"
import TransactionDataTable from "../dashboard-component/transaction-datatable/TransactionDataTable";
import { useDispatch } from "react-redux";
import { filterTrans } from "../../../redux/transaction-action"
import Receipt from "../dashboard-component/transaction-receipt/Receipt";
import { types } from "../../../redux/action"
import { dateNow } from '../../../services/helpers';
import moment from "moment";
import {Link} from "react-router-dom"
const Transaction = () => {
    let history = useHistory()
    const dispatch = useDispatch()


    useEffect(() => {
        isLogin()
        dispatch({type:types.ALL_NOT_LOADING})
        const dataFilter = {
            mutation:"none",
            transType:"none",
            startDate:moment().subtract(1, "week").format("YYYY-MM-DD"),
            endDate:dateNow(),
        }
        dispatch(filterTrans(dataFilter))
    }, [])

    const isLogin = () => {
        if (typeof Cookies.get("TOKEN_USER") === "undefined") {
            history.push("/");
            return true;
        }
    }
    return (
        <>
            <div className="row m-0">
                <div className="col-lg-2 p-0">
                    <Sidebar />
                </div>
                <div className="col-lg-7">
                    <Container>

                        <TitleSearcbar title="Dashboard" />
                        <div className="row">
                            <div className="col-12">
                                <div className="transaction-breadcrumb mt-4">
                                    <Link to="/dashboard" className="transaction-breadcrumb-link">Dashboard </Link>
                                    <span className="ms-1 transaction-breadcrumb-text"> ▶ Transaction</span>
                                </div>
                                
                                <TransactionDataTable/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="col-lg-3 b-left mt-3">
                    <div className="row p-3 mb-4">
                        <div className="col-lg-12">
                            <Receipt />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Transaction
