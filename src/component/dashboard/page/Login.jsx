import { Container } from "reactstrap"
import LoginCarousel from "../dashboard-component/login-carousel/LoginCarousel"
import LoginHeader from "../dashboard-component/login-header/LoginHeader"
import illustration1 from "../dashboard-component/login-carousel/img/login-illustration.svg"
import illustration2 from "../dashboard-component/login-carousel/img/login-illustration2.svg"
import illustration3 from "../dashboard-component/login-carousel/img/login-illustration3.svg"
import LoginForm from "../dashboard-component/login-form/LoginForm"
import { useEffect } from "react"
import Cookies from "js-cookie"
import { useHistory } from "react-router-dom"


const Login = () => {
    let history = useHistory()

    useEffect(() => {
        isLogin()
      }, [])
    
      const isLogin = () => {
        if(typeof Cookies.get("TOKEN_USER") !== "undefined") {
          history.push("/dashboard");
          return true;
        }
      }

    return (
        <div>
            <LoginHeader />
            <Container>
            <div className="row mt-5">
                <div className="col-lg-7 col-sm-12 mb-5">
                    <LoginCarousel img={[illustration1,illustration2,illustration3]}/>
                </div>
                <div className="col-lg-5 col-sm-12 mt-3">
                    <LoginForm />
                </div>
            </div>
            </Container>
        </div>
    )
}

export default Login
