import Sidebar from '../dashboard-component/sidebar/Sidebar'
import TableMissionList from '../dashboard-component/table-mission-list/TableMissionLists'
import TableOfferList from '../dashboard-component/table-offer-list/TableOfferLists'
import TableVoucherList from '../dashboard-component/table-voucher-list/TableVoucherLists'
import TablePromoList from '../dashboard-component/table-promo-list/TablePromoLists'
import TitleAndSearchbar from '../dashboard-component/title-searchbar/TitleAndSearchbar'
import { useDispatch,  useSelector } from 'react-redux'
import { tableMissionList, tableVoucherList } from  "../../../redux/voucher-action"
import { Spinner } from 'reactstrap';
import { useEffect } from "react"
import Cookies from "js-cookie"
import { useHistory } from "react-router-dom"
import { types } from "../../../redux/action"
const Voucher = () => {
    let history = useHistory()
    const dispatch = useDispatch()
    const missionloading = useSelector(state => state.isLoading1)
    const missionData = useSelector(state=> state.tableMissionList)
    const voucherloading = useSelector(state => state.isLoading3)
    const voucherData = useSelector(state=> state.tableVoucherList)
    const OfferData = useSelector(state=> state.tableOfferList)

    useEffect(() => {
        isLogin()
        dispatch({type:types.ALL_NOT_LOADING})
        dispatch(tableMissionList())
        dispatch(tableVoucherList())
    }, [])

    const isLogin = () => {
        if (typeof Cookies.get("TOKEN_USER") === "undefined") {
            history.push("/");
            return true;
        }
    }
    return (
        <>
            <div className="row m-0">
                <div className="col-lg-2 p-0">
                    <Sidebar />
                </div>
                <div className="col-lg-10 px-4">

                    <div className="row">
                        <div className="col-lg-10">
                            <TitleAndSearchbar title="Point & Reward" />
                        </div>
                        <div className="col-lg-2"></div>
                    </div>
                    {missionloading && 
                        <Spinner color="primary" children="" className="mt-3"/>
                    }
                    {!missionloading && missionData &&
                        <TableMissionList data={missionData}/>
                    }
                    {voucherloading && 
                        <Spinner color="primary" children="" className="mt-3"/>
                    }
                    {!voucherloading && OfferData &&
                        <TableOfferList data={OfferData}/>
                    }
                    {voucherloading && 
                        <Spinner color="primary" children="" className="mt-3"/>
                    }
                    {!voucherloading && voucherData &&
                        <TableVoucherList data={voucherData}/>
                    }
                    {/* <TablePromoList /> */}
                </div>
            </div>
        </>
    )
}

export default Voucher
