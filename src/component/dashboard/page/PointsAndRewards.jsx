import Sidebar from '../dashboard-component/sidebar/Sidebar'
import CardRanking from '../dashboard-component/card-ranking/CardRanking'
import "./position.scss"
import { useEffect } from "react"
import Cookies from "js-cookie"
import { useHistory } from "react-router-dom"
import TitleAndSearchbar from '../dashboard-component/title-searchbar/TitleAndSearchbar'
import TableRanking from '../dashboard-component/mission-table-ranking/TableRanking'
import TableUserCheckin from '../dashboard-component/mission-user-checkin/TableUserCheckin'
import TableMissionCompletion from '../dashboard-component/mission-table-mission-completion/TableMissionCompletion'
import { useDispatch, useSelector } from 'react-redux'
import { leaderboard } from "../../../redux/point-action"
import { Spinner } from 'reactstrap';
import { types } from "../../../redux/action"
const PointsAndRewards = () => {
    let history = useHistory()
    const dispatch = useDispatch()
    const leaderboardloading = useSelector(state => state.isLoading1)
    const topthreeData = useSelector(state => state.topthree)
    const leaderboardData = useSelector(state => state.leaderboard)

    useEffect(() => {
        isLogin()
        dispatch({type:types.ALL_NOT_LOADING})
        dispatch(leaderboard())
    }, [])
    const isLogin = () => {
        if (typeof Cookies.get("TOKEN_USER") === "undefined") {
            history.push("/");
            return true;
        }
    }

    return (
        <div>
            <div className="row m-0">
                <div className="col-lg-2 p-0">
                    <Sidebar />
                </div>
                <div className="col-lg-10 px-4">

                    <div className="row">
                        <div className="col-lg-10">
                            <TitleAndSearchbar title="Leaderboard" />
                        </div>
                        <div className="col-lg-2"></div>
                    </div>
                    {leaderboardloading &&
                        <Spinner color="primary" children="" className="mt-3" />
                    }
                    {!leaderboardloading && topthreeData && leaderboardData && <>
                        <CardRanking data={topthreeData} loading={leaderboardloading} />
                        <TableRanking data={leaderboardData} />
                    </>
                    }
                    <TableUserCheckin />
                    <TableMissionCompletion />
                </div>
            </div>
        </div>
    )
}

export default PointsAndRewards
