import axios from 'axios';
import Cookies from "js-cookie"
import {types} from "./action"

export const tableMissionList = () => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING1})
    const url = `${process.env.REACT_APP_API}/v1/mission`
    axios.get(url,headers)
    .then(res => {
        const data = res.data.map((data,index)=> {
            let {activity} = data
            let transactionName = activity
            let transaction
            (transactionName === "PAYMENT_QR") ? transaction = "Pay (QR Code) with Jalin when shopping in merchant partner" 
            : (transactionName === "TOP_UP") ? transaction = "Top Up E-Wallet" 
            : (transactionName === "TRANSFER") ? transaction = "Transfer to other Jalin Account" 
            : (transactionName === "PAYMENT_BILL") ? transaction = "Pay bill (electricity, water, telephone, internet)" 
            : (transactionName === "TRANSFER_DOMESTIC") ? transaction = "Transfer to other Bank" 
            : (transactionName === "PAYMENT_MOBILE_PHONE_DATA") ? transaction = "Top Up Mobile Data" 
            : transaction = "Top Up Mobile Credit";
            return {
                number:index+1,
                ...data,
                frequency: data.frequency+" times",
                activity:transaction
            }
        })
        dispatch({type: types.TABLEMISSIONLIST, data:data})
    })
    .catch(err => {
        console.log(err)
    })
}


export const tableVoucherList = () => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING2})
    const url = `${process.env.REACT_APP_API}/v1/voucher`
    axios.get(url,headers)
    .then(res => {
        let voucher=[]
        let offer = []
        let number
        let title

        res.data.forEach((element,i) => {
            (element.category === "VOUCHER") ? 
            voucher.push({element})  : offer.push({element})
        });
        voucher = voucher.map((data,i) => {
            (data.element.usage === "PAYMENT_MOBILE_PHONE_DATA") ? title = "Discount payment mobile data Tsel or Indosat or SmartFren, or Three quota" 
            : title = "Discount mobile credit any operator";
            
            number = i + 1
            return{
                ...data.element, number,title
            }
        })
        offer = offer.map((data,i)  => {
            (data.element.usage === "TRANSFER_DOMESTIC") ? title = "Free transfer to any bank" 
            : (data.element.usage === "TOP_UP") ? title = "Free top up e-wallet e-wallet admin fee" 
            : (data.element.usage === "PAYMENT_BILL_PLN") ? title = "Free electricity payment admin fee" 
            : (data.element.usage === "PAYMENT_BILL_PDAM") ? title = "Free PDAM payment admin fee" 
            : (data.element.usage === "PAYMENT_BILL_INTERNET") ? title = "Free internet provider payment admin fee" 
            : (data.element.usage === "PAYMENT_MOBILE_PHONE_CREDIT_25000") ? title = "Free IDR 25000 additional credit to any operator" 
            : (data.element.usage === "PAYMENT_MOBILE_PHONE_CREDIT_15000") ? title = "Free IDR 25000 additional credit to any operator" 
            : title = "Free 5GB additional internet data for telkomsel, indosat, smartfren, and three operators";
            
            number = i + 1
            return{
                ...data.element, number,title
            }
        })
        
        dispatch({type: types.TABLEVOUCHERLIST, data:voucher})
        dispatch({type: types.TABLEOFFERLIST, data:offer})
    })
    .catch(err => {
        console.log(err)
    })
}

export const addMission = (data) => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    const url = `${process.env.REACT_APP_API}/v1/mission` 
    axios.post(url,data,headers)
    .then(res => {
        dispatch(tableMissionList())
    })
    .catch(err => {
        console.log(err)
    })
}

export const editMission = (data) => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    const id = data.id
    const url = `${process.env.REACT_APP_API}/v1/mission/${id}` 
    axios.put(url,data,headers)
    .then(res => {
        dispatch(tableMissionList())
    })
    .catch(err => {
        console.log(err)
    })
}

export const deleteMission = (data) => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING1})
    const url = `${process.env.REACT_APP_API}/v1/mission/${data}` 
    axios.delete(url,headers)
    .then(res => {
        dispatch(tableMissionList())
    })
    .catch(err => {
        console.log(err)
    })
}

export const addOffer = (data) => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    const url = `${process.env.REACT_APP_API}/v1/voucher` 
    dispatch({type: types.CHANGE_LOADING2})
    axios.post(url,data,headers)
    .then(res => {
        dispatch(tableVoucherList())
    })
    .catch(err => {
        console.log(err)
    })
}

export const editOffer = (data) => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    const id = data.id
    const url = `${process.env.REACT_APP_API}/v1/voucher/${id}` 
    axios.put(url,data,headers)
    .then(res => {
        dispatch(tableVoucherList())
    })
    .catch(err => {
        console.log(err)
    })
}

export const deleteOffer = (data) => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING2})
    const url = `${process.env.REACT_APP_API}/v1/voucher/${data}` 
    axios.delete(url,headers)
    .then(res => {
        dispatch(tableVoucherList())
    })
    .catch(err => {
        console.log(err)
    })
}