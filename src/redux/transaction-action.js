import axios from 'axios';
import Cookies from "js-cookie"
import moment from 'moment';
import {types} from "./action"

export const transactionData  = () => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING1})
    const url = `${process.env.REACT_APP_API}/v1/transactions/all`
    axios.get(url,headers)
    .then(res => {
        const dataTrans = res.data.map((data)=> {
            let {transactionName, amount,transactionType,transactionId,transactionTime,corporateName,transactionDate} = data
            let transaction
            let cash 
            (transactionName === "PAYMENT_QR") ? transaction = "Pembayaran QR" 
            : (transactionName === "TOP_UP") ? transaction = "Top Up E-Wallet" 
            : (transactionName === "TRANSFER") ? transaction = "Transfer" 
            : (transactionName === "PAYMENT_BILL") ? transaction = "Pembayaran Tagihan" 
            : (transactionName === "TRANSFER_DOMESTIC") ? transaction = "Transfer Domestik" 
            : transaction = "Pengisian Pulsa";

            (transactionType === "C") ?
            cash = "-RP. "+parseFloat(amount).toLocaleString('de-DE')  :  cash = "+RP. "+parseFloat(amount).toLocaleString('de-DE')  ;
            
            return {
                id: transactionId,
                Transaction: transaction,
                Destination: corporateName,
                Date: transactionDate,
                Time:transactionTime.substring(0,5),
                CashFlow:cash
            }
        })
        dispatch({type:types.TRANSACTION, dataTrans:dataTrans})
    })
    .catch(err => {
        console.log(err)
    })
}

export const filterTrans = (data) => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    let url
    const today = moment(data.startDate).format("YYYY-MM-DD")
    const lastMonth = moment(data.endDate).format("YYYY-MM-DD")

    dispatch({type: types.CHANGE_LOADING1})
    if(data.mutation === "none" && data.transType === "none"){
     url = `${process.env.REACT_APP_API}/v1/transactions?start=${today}&end=${lastMonth}&size=1000`
    } else if(data.mutation !== "none" && data.transType === "none"){
        url = `${process.env.REACT_APP_API}/v1/transactions?start=${today}&end=${lastMonth}&type=${data.mutation}&size=1000`
    } else if(data.mutation === "none" && data.transType !== "none"){
        url = `${process.env.REACT_APP_API}/v1/transactions?start=${today}&end=${lastMonth}&name=${data.transType}&size=1000`
    } else {
        url = `${process.env.REACT_APP_API}/v1/transactions?start=${today}&end=${lastMonth}&name=${data.transType}&type=${data.mutation}&size=1000`
    }
    
    axios.get(url,headers)
    .then(res => {
        const dataTrans = res.data.transactionList.map((data)=> {
            let {transactionName, amount,transactionType,transactionId,transactionTime,corporateName} = data
            let transaction
            let cash 
            (transactionName === "PAYMENT_QR") ? transaction = "Pembayaran QR" 
            : (transactionName === "TOP_UP") ? transaction = "Top Up E-Wallet" 
            : (transactionName === "TRANSFER") ? transaction = "Transfer" 
            : (transactionName === "PAYMENT_BILL") ? transaction = "Pembayaran Tagihan" 
            : (transactionName === "TRANSFER_DOMESTIC") ? transaction = "Transfer Domestik" 
            : (transactionName === "PAYMENT_MOBILE_PHONE_DATA") ? transaction = "Isi Paket Data" 
            : transaction = "Isi Pulsa";

            (transactionType === "C") ?
            cash = "-RP. "+parseFloat(amount).toLocaleString('de-DE')  :  cash = "+RP. "+parseFloat(amount).toLocaleString('de-DE')  ;
            return {
                id: transactionId,
                Transaction: transaction,
                Destination: corporateName,
                Date: data.transactionDate,
                Time:transactionTime.substring(0,5),
                CashFlow:cash
            }
        })
        dispatch({type:types.TRANSACTION, dataTrans:dataTrans})
    })
    .catch(err => {
        console.log(err)
    })
}

export const transactionReceipt  = (data) => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING2})
    const url = `${process.env.REACT_APP_API}/v1/transactions/${data.id}`
    axios.get(url,headers)
    .then(res => {
        const data =  res.data
        let { amount, transactionName,transactionTime,transactionType } = data
        let transaction
        let cash 
        (transactionName === "PAYMENT_QR") ? transaction = "Pembayaran QR" 
        : (transactionName === "TOP_UP") ? transaction = "Top Up E-Wallet" 
        : (transactionName === "TRANSFER") ? transaction = "Transfer" 
        : (transactionName === "PAYMENT_BILL") ? transaction = "Pembayaran Tagihan" 
        : (transactionName === "TRANSFER_DOMESTIC") ? transaction = "Transfer Domestik" 
        : (transactionName === "PAYMENT_MOBILE_PHONE_DATA") ? transaction = "Isi Paket Data" 
        : transaction = "Isi Pulsa";

        (transactionType === "C") ?
        cash = "RP. "+parseFloat(amount).toLocaleString('de-DE')  :  cash = "RP. "+parseFloat(amount).toLocaleString('de-DE')  ;
        
        const dataTrans = {
            ...data,
            amount:cash,
            transactionName:transaction,
            transactionTime:transactionTime.substring(0,5),
        }
        // console.log(dataTrans);
        dispatch({type:types.TRANSACTIONRECEIPT, data:dataTrans})
    })
    .catch(err => {
        console.log(err)
    })
}