import { types } from './action'

const globalState = {
    loading:false,
    isLoading1: false,
    isLoading2: false,
    isLoading3: false,
    isLoading4: false,
    isLoading5: false,
    cardData:"",
    cash:null,
    calendar: "",
    dashboardTransData:null,
    dashboardCheckIn:null,
    transaction:null,
    transactionreceipt:null,
    topthree:null,
    leaderboard:null,
    tableCheckin:null,
    tableMission:null,
    tableMissionList:null,
    tableVoucherList:null,
    tableOfferList:null,
}

const rootReducer = (state=globalState, action) => {
    switch (action.type) {
        case types.LOADING:
            return{
                ...state,
                loading: true
            }
        case types.NOT_LOADING:
            return{
                ...state,
                loading: false
            }
        case types.NOT_LOADING2:
            return{
                ...state,
                loading2: false
            }
        case types.CHANGE_LOADING1:
            return{
                ...state,
                isLoading1: true
            }
        case types.CHANGE_LOADING2:
            return{
                ...state,
                isLoading2: true
            }
        case types.CHANGE_LOADING3:
            return{
                ...state,
                isLoading3: true
            }
        case types.NOT_LOADING3:
        return{
            ...state,
            isLoading3: false
        }
        case types.CHANGE_LOADING4:
            return{
                ...state,
                isLoading4: true
            }
        case types.CHANGE_LOADING5:
            return{
                ...state,
                isLoading5: true
            }
        case types.ALL_NOT_LOADING5:
            return{
                ...state,
                loading: false,
                isLoading1: false,
                isLoading2: false,
                isLoading3: false,
                isLoading4: false,
                isLoading5: false
            }
        case types.CARDDATA:
            return{
                ...state,
                cardData: action.carddata,
                isLoading1: false
            }
        case types.CASHIN:
            return{
                ...state,
                cash: action.data,
                isLoading5: false
            }
        case types.CALENDAR:
            return{
                ...state,
                calendar: action.data,
                isLoading2: false
            }
        case types.DASHBOARDTRANS:
            return{
                ...state,
                dashboardTransData: action.dataTrans,
                isLoading3: false
            }
        case types.DASHBOARDCHECKIN:
            return{
                ...state,
                dashboardCheckIn: action.data,
                isLoading4: false
            }
        case types.TRANSACTION:
            return{
                ...state,
                transaction: action.dataTrans,
                isLoading1: false
            }
        case types.TRANSACTIONRECEIPT:
            return{
                ...state,
                transactionreceipt: action.data,
                isLoading2:false
            }
        case types.TOPTHREE:
            return{
                ...state,
                topthree: action.data,
                isLoading1: false
            }
        case types.LEADERBOARD:
            return{
                ...state,
                leaderboard: action.data,
                isLoading1: false
            }
        case types.TABLECHECKIN:
            return{
                ...state,
                tableCheckin: action.data,
                isLoading2: false
            }
        case types.TABLEMISSION:
            return{
                ...state,
                tableMission: action.data,
                isLoading3: false
            }
        case types.TABLEMISSIONLIST:
            return{
                ...state,
                tableMissionList: action.data,
                isLoading1: false
            }
        case types.TABLEVOUCHERLIST:
            return{
                ...state,
                tableVoucherList: action.data,
            }
        case types.TABLEOFFERLIST:
            return{
                ...state,
                tableOfferList: action.data,
                isLoading2: false
            }
        
        default:
            return state
    }
}

export default rootReducer
