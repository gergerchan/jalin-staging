import axios from 'axios';
import Cookies from "js-cookie"
import moment from 'moment';

export const types = {
    LOADING: "LOADING",
    CHANGE_LOADING1: "CHANGE_LOADING1",
    NOT_LOADING: "NOT_LOADING",
    CHANGE_LOADING2: "CHANGE_LOADING2",
    NOT_LOADING2: "NOT_LOADING3",
    CHANGE_LOADING3: "CHANGE_LOADING3",
    NOT_LOADING3: "NOT_LOADING3",
    CHANGE_LOADING4: "CHANGE_LOADING4",
    CHANGE_LOADING5: "CHANGE_LOADING5",
    ALL_NOT_LOADING: "ALL_NOT_LOADING",
    CARDDATA : "CARDDATA",
    CASHIN : "CASHIN",
    CALENDAR : "CALENDAR",
    DASHBOARDTRANS : "DASHBOARDTRANS",
    DASHBOARDCHECKIN : "DASHBOARDCHECKIN",
    TRANSACTION : "TRANSACTION",
    TRANSACTIONRECEIPT : "TRANSACTIONRECEIPT",
    TOPTHREE : "TOPTHREE",
    LEADERBOARD : "LEADERBOARD",
    TABLECHECKIN : "TABLECHECKIN",
    TABLEMISSION : "TABLEMISSION",
    TABLEMISSIONLIST : "TABLEMISSIONLIST",
    TABLEVOUCHERLIST : "TABLEVOUCHERLIST",
    TABLEOFFERLIST : "TABLEOFFERLIST",
    
}

export const DashboardCard = () => (dispatch) => {
    
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING1})
    const actcust = `${process.env.REACT_APP_API}/v1/activeCustomers`
    const checkin = `${process.env.REACT_APP_API}/v1/check-in/today`
    const missiontaken = `${process.env.REACT_APP_API}/v1/missionTaken`
    const totalmission = `${process.env.REACT_APP_API}/v1/missionComplete`
    const req1 = axios.get(actcust,headers)
    const req2 = axios.get(checkin,headers)
    const req3 = axios.get(missiontaken,headers)
    const req4 = axios.get(totalmission,headers)

    axios.all([req1,req2,req3,req4])
    .then(axios.spread((...res) => {
        const percent =  Math.floor(res[3].data / res[2].data * 100)
        dispatch({type: types.CARDDATA, carddata:{
            activecust:res[0].data,
            mission:res[1].data,
            MissionTaken:res[2].data,
            missionComplete:res[3].data,
            percent:percent
        }})
    }))
    .catch(err => {
        console.error(err);
      });
}

export const DashboardCash = () => (dispatch) => {
    dispatch({type: types.CHANGE_LOADING5})
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    const url = `${process.env.REACT_APP_API}/v1/cash`
    axios.get(url,headers)
    .then(res => {
        dispatch({type: types.CASHIN, data:res.data})
    })
    .catch(err => {
        console.log(err)
    })
}

export const CalendarData = () => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING2})
    const url = `${process.env.REACT_APP_API}/v1/calendarData`
    axios.get(url,headers)
    .then(res => {
        const checkin = res.data.map((data)=> {
            let {checkins,start,end} = data
            return {
                title: checkins+" Checkin",
                start: start,
                end: end,
                hexColor: "#3EB7A1"
            }
        })
        const mission = res.data.map((data)=> {
            let {missions,start,end} = data
            return {
                title: missions+" Mission",
                start: start,
                end: end,
                hexColor: "#E15E54"
            }
        })
        const data =[ ...checkin, ...mission]
        dispatch({type: types.CALENDAR, data:data})
    })
    .catch(err => {
        console.log(err)
    })
}

export const dashboardRecentTrans = () => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING3})
    const today = moment().format("YYYY-MM-DD")
    const lastMonth = moment().subtract(1,"month").format("YYYY-MM-DD")
    const url = `${process.env.REACT_APP_API}/v1/transactions?start=${lastMonth}&end=${today}&sort=transactionId,desc`
    axios.get(url,headers)
    .then(res => {
        const dataTrans = res.data.transactionList.map((data)=> {
            let {transactionName, amount,transactionType,transactionId,transactionTime,corporateName} = data
            let transaction
            let cash 
            (transactionName === "PAYMENT_QR") ? transaction = "Pembayaran QR" 
            : (transactionName === "TOP_UP") ? transaction = "Top Up E-Wallet" 
            : (transactionName === "TRANSFER") ? transaction = "Transfer" 
            : (transactionName === "PAYMENT_BILL") ? transaction = "Pembayaran Tagihan" 
            : (transactionName === "TRANSFER_DOMESTIC") ? transaction = "Transfer Domestik" 
            : (transactionName === "PAYMENT_MOBILE_PHONE_DATA") ? transaction = "Isi Paket Data" 
            : transaction = "Isi Pulsa";

            (transactionType === "C") ?
            cash = "-RP. "+parseFloat(amount).toLocaleString('de-DE')  :  cash = "+RP. "+parseFloat(amount).toLocaleString('de-DE')  ;
            return {
                id: transactionId,
                Transaction: transaction,
                Destination: corporateName,
                Time:transactionTime.substring(0,5),
                CashFlow:cash
            }
        })
        dispatch({type:types.DASHBOARDTRANS, dataTrans:dataTrans})
    })
    .catch(err => {
        console.log(err)
    })
}

export const dashboardCheckin = () => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING4})
    const url = `${process.env.REACT_APP_API}/v1/userAppCheckin/?filter=today`
    axios.get(url,headers)
    .then(res => {
        dispatch({type: types.DASHBOARDCHECKIN, data:res.data})
    })
    .catch(err => {
        console.log(err)
    })
}
