import axios from 'axios';
import Cookies from "js-cookie"
import {types} from "./action"

export const leaderboard = () => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    let string
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING1})
    const url = `${process.env.REACT_APP_API}/v1/leaderboardData`
    axios.get(url,headers)
    .then(res => {
        const top =  res.data.topThree.map(data => {
            string = data.jalinId.includes("-") ? data.jalinId.split("-") : data.jalinId.split(".")
            string = string[string.length-1]
            return { ...data, jalinId:string}
        })
        
        const leaderboard = res.data.listLeaderboard.map(data => {
            string = data.jalinId.includes("-") ? data.jalinId.split("-") : data.jalinId.split(".")
            string = string[string.length-1]
            return { ...data, jalinId:string}
        })
        dispatch({type: types.TOPTHREE, data:top})
        dispatch({type: types.LEADERBOARD, data:leaderboard})
    })
    .catch(err => {
        console.log(err)
    })
}

export const tableCheckin = (data) => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING2})
    let url
    let string
    data ? 
    url = `${process.env.REACT_APP_API}/v1/userAppCheckin/?startDate=${data.start}&endDate=${data.end}` :
    url = `${process.env.REACT_APP_API}/v1/userAppCheckin`
    axios.get(url,headers)
    .then(res => {
        const data = res.data.map(data => {
            string = data.cid.includes("-") ? data.cid.split("-") : data.cid.split(".")
            string = string[string.length-1]
            return { ...data, cid:string}
        })
        dispatch({type: types.TABLECHECKIN, data:data})
    })
    .catch(err => {
        console.log(err)
    })
}

export const tableMissionCompletion= (data) => (dispatch) => {
    const token = Cookies.get("TOKEN_USER")
    const headers = {headers: {
        'Authorization': `Bearer ${token}` 
    }}
    dispatch({type: types.CHANGE_LOADING3})
    let url
    let string
    data ? 
    url = `${process.env.REACT_APP_API}/v1/userMissionCompletion/?startDate=${data.start}&endDate=${data.end}` :
    url = `${process.env.REACT_APP_API}/v1/userMissionCompletion`
    axios.get(url,headers)
    .then(res => {
        const data = res.data.map((data)=> {
            
            let {cid,date,fullName,missionTransaction,pointsUnlocked,status,timeMark} = data
            let transactionName = missionTransaction
            let transaction
            (transactionName === "PAYMENT_QR") ? transaction = "Pembayaran QR" 
            : (transactionName === "TOP_UP") ? transaction = "Top Up E-Wallet" 
            : (transactionName === "TRANSFER") ? transaction = "Transfer" 
            : (transactionName === "PAYMENT_BILL") ? transaction = "Pembayaran Tagihan" 
            : (transactionName === "TRANSFER_DOMESTIC") ? transaction = "Transfer Domestik" 
            : transaction = "Pengisian Pulsa";
            string = cid.includes("-") ? cid.split("-") : cid.split(".")
            string = string[string.length-1]
            return {
                cid: string,
                date: date,
                missionTransaction:transaction,
                fullName: fullName,
                pointsUnlocked: pointsUnlocked,
                status:status,
                timeMark: timeMark
            }
        })
        dispatch({type: types.TABLEMISSION, data:data})
    })
    .catch(err => {
        console.log(err)
    })
}