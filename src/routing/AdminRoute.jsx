import { Route ,Switch} from "react-router-dom";
import PointsAndRewards from "../component/dashboard/page/PointsAndRewards";
import Login from "../component/dashboard/page/Login";
import DashboardHome from "../component/dashboard/page/Home"
import Transaction from "../component/dashboard/page/Transaction"
import Voucher from "../component/dashboard/page/Voucher"
import ScrollToTop from "./ScrollToTop"

const AdminRoute = () => {
    return (
        <>
          <Switch>
            <Route exact path="/leaderboard">
                <ScrollToTop/>
                <PointsAndRewards />
            </Route>
            <Route path='/leaderboard/:name'>
    		    <PointsAndRewards />
    	    </Route>
            <Route exact path="/">
                <ScrollToTop/>
                <Login />
            </Route>
            <Route exact path="/dashboard">
                <ScrollToTop/>
                <DashboardHome />
            </Route>
            <Route exact path="/transaction">
                <ScrollToTop/>
                <Transaction />
            </Route>
            <Route exact path="/points-and-rewards">
                <ScrollToTop/>
                <Voucher />
            </Route>
          </Switch>
        </>
    )
}

export default AdminRoute
